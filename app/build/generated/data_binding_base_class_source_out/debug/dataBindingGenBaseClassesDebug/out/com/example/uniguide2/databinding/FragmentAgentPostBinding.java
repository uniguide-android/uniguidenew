package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.AgentPostFragment;
import com.example.uniguide2.data.News;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentAgentPostBinding extends ViewDataBinding {
  @NonNull
  public final View divider5;

  @NonNull
  public final ImageView imageView4;

  @NonNull
  public final MaterialCardView materialCardView;

  @NonNull
  public final Button postButton;

  @NonNull
  public final TextInputLayout textInputLayout;

  @NonNull
  public final TextInputLayout textInputLayout1;

  @NonNull
  public final TextInputEditText uniNewsContent;

  @NonNull
  public final TextInputEditText uniNewsTitle;

  @NonNull
  public final TextView usernameTextview;

  @Bindable
  protected News mNews;

  @Bindable
  protected AgentPostFragment mCallback;

  protected FragmentAgentPostBinding(Object _bindingComponent, View _root, int _localFieldCount,
      View divider5, ImageView imageView4, MaterialCardView materialCardView, Button postButton,
      TextInputLayout textInputLayout, TextInputLayout textInputLayout1,
      TextInputEditText uniNewsContent, TextInputEditText uniNewsTitle, TextView usernameTextview) {
    super(_bindingComponent, _root, _localFieldCount);
    this.divider5 = divider5;
    this.imageView4 = imageView4;
    this.materialCardView = materialCardView;
    this.postButton = postButton;
    this.textInputLayout = textInputLayout;
    this.textInputLayout1 = textInputLayout1;
    this.uniNewsContent = uniNewsContent;
    this.uniNewsTitle = uniNewsTitle;
    this.usernameTextview = usernameTextview;
  }

  public abstract void setNews(@Nullable News news);

  @Nullable
  public News getNews() {
    return mNews;
  }

  public abstract void setCallback(@Nullable AgentPostFragment callback);

  @Nullable
  public AgentPostFragment getCallback() {
    return mCallback;
  }

  @NonNull
  public static FragmentAgentPostBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_agent_post, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentAgentPostBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentAgentPostBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_agent_post, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentAgentPostBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_agent_post, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentAgentPostBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentAgentPostBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_agent_post, null, false, component);
  }

  public static FragmentAgentPostBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentAgentPostBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentAgentPostBinding)bind(component, view, com.example.uniguide2.R.layout.fragment_agent_post);
  }
}
