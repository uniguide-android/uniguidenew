package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.AgentSignUpFragment;
import com.example.uniguide2.data.Agent;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentAgentSignUpBinding extends ViewDataBinding {
  @NonNull
  public final TextInputEditText NameEt;

  @NonNull
  public final TextInputEditText addressEt;

  @NonNull
  public final TextInputEditText confirmPasswordEt;

  @NonNull
  public final TextInputEditText descriptionEt;

  @NonNull
  public final TextInputEditText emailEt;

  @NonNull
  public final Guideline guideline3;

  @NonNull
  public final Guideline guideline4;

  @NonNull
  public final Guideline guideline5;

  @NonNull
  public final MaterialButton loginButton;

  @NonNull
  public final TextInputEditText passwordEt;

  @NonNull
  public final TextInputEditText phoneNumberEt;

  @NonNull
  public final TextView signUpTv;

  @NonNull
  public final TextInputLayout textInputLayout10;

  @NonNull
  public final TextInputLayout textInputLayout11;

  @NonNull
  public final TextInputLayout textInputLayout3;

  @NonNull
  public final TextInputLayout textInputLayout4;

  @NonNull
  public final TextInputLayout textInputLayout5;

  @NonNull
  public final TextInputLayout textInputLayout6;

  @NonNull
  public final TextInputLayout textInputLayout9;

  @NonNull
  public final TextInputEditText userNameEt;

  @Bindable
  protected Agent mAgent;

  @Bindable
  protected AgentSignUpFragment mCallback;

  protected FragmentAgentSignUpBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextInputEditText NameEt, TextInputEditText addressEt, TextInputEditText confirmPasswordEt,
      TextInputEditText descriptionEt, TextInputEditText emailEt, Guideline guideline3,
      Guideline guideline4, Guideline guideline5, MaterialButton loginButton,
      TextInputEditText passwordEt, TextInputEditText phoneNumberEt, TextView signUpTv,
      TextInputLayout textInputLayout10, TextInputLayout textInputLayout11,
      TextInputLayout textInputLayout3, TextInputLayout textInputLayout4,
      TextInputLayout textInputLayout5, TextInputLayout textInputLayout6,
      TextInputLayout textInputLayout9, TextInputEditText userNameEt) {
    super(_bindingComponent, _root, _localFieldCount);
    this.NameEt = NameEt;
    this.addressEt = addressEt;
    this.confirmPasswordEt = confirmPasswordEt;
    this.descriptionEt = descriptionEt;
    this.emailEt = emailEt;
    this.guideline3 = guideline3;
    this.guideline4 = guideline4;
    this.guideline5 = guideline5;
    this.loginButton = loginButton;
    this.passwordEt = passwordEt;
    this.phoneNumberEt = phoneNumberEt;
    this.signUpTv = signUpTv;
    this.textInputLayout10 = textInputLayout10;
    this.textInputLayout11 = textInputLayout11;
    this.textInputLayout3 = textInputLayout3;
    this.textInputLayout4 = textInputLayout4;
    this.textInputLayout5 = textInputLayout5;
    this.textInputLayout6 = textInputLayout6;
    this.textInputLayout9 = textInputLayout9;
    this.userNameEt = userNameEt;
  }

  public abstract void setAgent(@Nullable Agent agent);

  @Nullable
  public Agent getAgent() {
    return mAgent;
  }

  public abstract void setCallback(@Nullable AgentSignUpFragment callback);

  @Nullable
  public AgentSignUpFragment getCallback() {
    return mCallback;
  }

  @NonNull
  public static FragmentAgentSignUpBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_agent_sign_up, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentAgentSignUpBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentAgentSignUpBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_agent_sign_up, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentAgentSignUpBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_agent_sign_up, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentAgentSignUpBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentAgentSignUpBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_agent_sign_up, null, false, component);
  }

  public static FragmentAgentSignUpBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentAgentSignUpBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentAgentSignUpBinding)bind(component, view, com.example.uniguide2.R.layout.fragment_agent_sign_up);
  }
}
