package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.CareerFragment;
import com.example.uniguide2.data.Career;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentCareerBinding extends ViewDataBinding {
  @NonNull
  public final Button addButton;

  @NonNull
  public final TextInputLayout codeEditText;

  @NonNull
  public final TextInputEditText codeText;

  @NonNull
  public final Button deleteButton;

  @NonNull
  public final EditText descrText;

  @NonNull
  public final Guideline guideline;

  @NonNull
  public final Guideline guideline3;

  @NonNull
  public final Guideline guideline4;

  @NonNull
  public final Guideline guideline5;

  @NonNull
  public final Guideline guideline6;

  @NonNull
  public final TextInputLayout jobsEditText;

  @NonNull
  public final TextInputEditText jobsText;

  @NonNull
  public final TextInputLayout nameEditText;

  @NonNull
  public final TextInputEditText nameText;

  @NonNull
  public final Button searchButton;

  @NonNull
  public final TextInputLayout searchEditText;

  @NonNull
  public final TextInputEditText searchText;

  @NonNull
  public final TextView textView3;

  @NonNull
  public final TextView textView7;

  @NonNull
  public final Button updateButton;

  @Bindable
  protected Career mGroup;

  @Bindable
  protected CareerFragment mCallback;

  protected FragmentCareerBinding(Object _bindingComponent, View _root, int _localFieldCount,
      Button addButton, TextInputLayout codeEditText, TextInputEditText codeText,
      Button deleteButton, EditText descrText, Guideline guideline, Guideline guideline3,
      Guideline guideline4, Guideline guideline5, Guideline guideline6,
      TextInputLayout jobsEditText, TextInputEditText jobsText, TextInputLayout nameEditText,
      TextInputEditText nameText, Button searchButton, TextInputLayout searchEditText,
      TextInputEditText searchText, TextView textView3, TextView textView7, Button updateButton) {
    super(_bindingComponent, _root, _localFieldCount);
    this.addButton = addButton;
    this.codeEditText = codeEditText;
    this.codeText = codeText;
    this.deleteButton = deleteButton;
    this.descrText = descrText;
    this.guideline = guideline;
    this.guideline3 = guideline3;
    this.guideline4 = guideline4;
    this.guideline5 = guideline5;
    this.guideline6 = guideline6;
    this.jobsEditText = jobsEditText;
    this.jobsText = jobsText;
    this.nameEditText = nameEditText;
    this.nameText = nameText;
    this.searchButton = searchButton;
    this.searchEditText = searchEditText;
    this.searchText = searchText;
    this.textView3 = textView3;
    this.textView7 = textView7;
    this.updateButton = updateButton;
  }

  public abstract void setGroup(@Nullable Career group);

  @Nullable
  public Career getGroup() {
    return mGroup;
  }

  public abstract void setCallback(@Nullable CareerFragment callback);

  @Nullable
  public CareerFragment getCallback() {
    return mCallback;
  }

  @NonNull
  public static FragmentCareerBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_career, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentCareerBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentCareerBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_career, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentCareerBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_career, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentCareerBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentCareerBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_career, null, false, component);
  }

  public static FragmentCareerBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentCareerBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentCareerBinding)bind(component, view, com.example.uniguide2.R.layout.fragment_career);
  }
}
