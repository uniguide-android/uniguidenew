package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.data.Career;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentCareerDetailBinding extends ViewDataBinding {
  @NonNull
  public final View divider;

  @NonNull
  public final TextView textView10;

  @NonNull
  public final TextView textView17;

  @NonNull
  public final TextView textView4;

  @NonNull
  public final TextView textView6;

  @NonNull
  public final TextView textView8;

  @Bindable
  protected Career mGroup;

  protected FragmentCareerDetailBinding(Object _bindingComponent, View _root, int _localFieldCount,
      View divider, TextView textView10, TextView textView17, TextView textView4,
      TextView textView6, TextView textView8) {
    super(_bindingComponent, _root, _localFieldCount);
    this.divider = divider;
    this.textView10 = textView10;
    this.textView17 = textView17;
    this.textView4 = textView4;
    this.textView6 = textView6;
    this.textView8 = textView8;
  }

  public abstract void setGroup(@Nullable Career group);

  @Nullable
  public Career getGroup() {
    return mGroup;
  }

  @NonNull
  public static FragmentCareerDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_career_detail, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentCareerDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentCareerDetailBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_career_detail, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentCareerDetailBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_career_detail, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentCareerDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentCareerDetailBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_career_detail, null, false, component);
  }

  public static FragmentCareerDetailBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentCareerDetailBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentCareerDetailBinding)bind(component, view, com.example.uniguide2.R.layout.fragment_career_detail);
  }
}
