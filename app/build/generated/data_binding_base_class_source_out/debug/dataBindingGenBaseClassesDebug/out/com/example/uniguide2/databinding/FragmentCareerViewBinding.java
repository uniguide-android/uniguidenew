package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentCareerViewBinding extends ViewDataBinding {
  @NonNull
  public final RecyclerView careerOptions;

  @NonNull
  public final TextView textView2;

  protected FragmentCareerViewBinding(Object _bindingComponent, View _root, int _localFieldCount,
      RecyclerView careerOptions, TextView textView2) {
    super(_bindingComponent, _root, _localFieldCount);
    this.careerOptions = careerOptions;
    this.textView2 = textView2;
  }

  @NonNull
  public static FragmentCareerViewBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_career_view, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentCareerViewBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentCareerViewBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_career_view, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentCareerViewBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_career_view, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentCareerViewBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentCareerViewBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_career_view, null, false, component);
  }

  public static FragmentCareerViewBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentCareerViewBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentCareerViewBinding)bind(component, view, com.example.uniguide2.R.layout.fragment_career_view);
  }
}
