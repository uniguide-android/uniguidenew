package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.LoginFragment;
import com.example.uniguide2.data.User;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentLoginBinding extends ViewDataBinding {
  @NonNull
  public final MaterialButton agent;

  @NonNull
  public final Guideline guideline;

  @NonNull
  public final ImageView imageView2;

  @NonNull
  public final MaterialButton loginButton;

  @NonNull
  public final TextInputEditText passwordEditText;

  @NonNull
  public final TextInputLayout passwordTextInput;

  @NonNull
  public final MaterialButton signupUniversityButton2;

  @NonNull
  public final MaterialButton student;

  @NonNull
  public final TextInputLayout textInputLayout;

  @NonNull
  public final TextView textView2;

  @NonNull
  public final TextInputEditText usernameEditText;

  @Bindable
  protected User mUser;

  @Bindable
  protected LoginFragment mCallback;

  protected FragmentLoginBinding(Object _bindingComponent, View _root, int _localFieldCount,
      MaterialButton agent, Guideline guideline, ImageView imageView2, MaterialButton loginButton,
      TextInputEditText passwordEditText, TextInputLayout passwordTextInput,
      MaterialButton signupUniversityButton2, MaterialButton student,
      TextInputLayout textInputLayout, TextView textView2, TextInputEditText usernameEditText) {
    super(_bindingComponent, _root, _localFieldCount);
    this.agent = agent;
    this.guideline = guideline;
    this.imageView2 = imageView2;
    this.loginButton = loginButton;
    this.passwordEditText = passwordEditText;
    this.passwordTextInput = passwordTextInput;
    this.signupUniversityButton2 = signupUniversityButton2;
    this.student = student;
    this.textInputLayout = textInputLayout;
    this.textView2 = textView2;
    this.usernameEditText = usernameEditText;
  }

  public abstract void setUser(@Nullable User user);

  @Nullable
  public User getUser() {
    return mUser;
  }

  public abstract void setCallback(@Nullable LoginFragment callback);

  @Nullable
  public LoginFragment getCallback() {
    return mCallback;
  }

  @NonNull
  public static FragmentLoginBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_login, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentLoginBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentLoginBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_login, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentLoginBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_login, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentLoginBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentLoginBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_login, null, false, component);
  }

  public static FragmentLoginBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentLoginBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentLoginBinding)bind(component, view, com.example.uniguide2.R.layout.fragment_login);
  }
}
