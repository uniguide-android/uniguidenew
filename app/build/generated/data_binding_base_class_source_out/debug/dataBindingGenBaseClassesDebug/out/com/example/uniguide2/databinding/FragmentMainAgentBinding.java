package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.MainAgentFragment;
import com.example.uniguide2.data.Agent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentMainAgentBinding extends ViewDataBinding {
  @NonNull
  public final ImageView cardviewImage;

  @NonNull
  public final TextView contentTextview;

  @NonNull
  public final TextView descriptionTv;

  @NonNull
  public final View divider;

  @NonNull
  public final FloatingActionButton fab;

  @NonNull
  public final Guideline guideline2;

  @NonNull
  public final TextView textView13;

  @NonNull
  public final TextView textView14;

  @NonNull
  public final TextView textView15;

  @NonNull
  public final TextView uniNewsContent;

  @NonNull
  public final TextView uniNewsTitle;

  @NonNull
  public final TextView usernameTextview;

  @Bindable
  protected Agent mProfile;

  @Bindable
  protected MainAgentFragment mCallback;

  protected FragmentMainAgentBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ImageView cardviewImage, TextView contentTextview, TextView descriptionTv, View divider,
      FloatingActionButton fab, Guideline guideline2, TextView textView13, TextView textView14,
      TextView textView15, TextView uniNewsContent, TextView uniNewsTitle,
      TextView usernameTextview) {
    super(_bindingComponent, _root, _localFieldCount);
    this.cardviewImage = cardviewImage;
    this.contentTextview = contentTextview;
    this.descriptionTv = descriptionTv;
    this.divider = divider;
    this.fab = fab;
    this.guideline2 = guideline2;
    this.textView13 = textView13;
    this.textView14 = textView14;
    this.textView15 = textView15;
    this.uniNewsContent = uniNewsContent;
    this.uniNewsTitle = uniNewsTitle;
    this.usernameTextview = usernameTextview;
  }

  public abstract void setProfile(@Nullable Agent profile);

  @Nullable
  public Agent getProfile() {
    return mProfile;
  }

  public abstract void setCallback(@Nullable MainAgentFragment callback);

  @Nullable
  public MainAgentFragment getCallback() {
    return mCallback;
  }

  @NonNull
  public static FragmentMainAgentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_main_agent, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentMainAgentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentMainAgentBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_main_agent, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentMainAgentBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_main_agent, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentMainAgentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentMainAgentBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_main_agent, null, false, component);
  }

  public static FragmentMainAgentBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentMainAgentBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentMainAgentBinding)bind(component, view, com.example.uniguide2.R.layout.fragment_main_agent);
  }
}
