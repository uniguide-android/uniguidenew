package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.MainUniversityFragment;
import com.example.uniguide2.data.University;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentMainUniversityBinding extends ViewDataBinding {
  @NonNull
  public final ImageView cardviewImage;

  @NonNull
  public final TextView contentTextview;

  @NonNull
  public final TextView descriptionTv;

  @NonNull
  public final View divider;

  @NonNull
  public final View divider4;

  @NonNull
  public final FloatingActionButton fab;

  @NonNull
  public final Guideline guideline7;

  @NonNull
  public final TextView textView11;

  @NonNull
  public final TextView textView12;

  @NonNull
  public final TextView uniNewsTitle;

  @NonNull
  public final TextView usernameTextview;

  @Bindable
  protected University mUni;

  @Bindable
  protected MainUniversityFragment mCallback;

  protected FragmentMainUniversityBinding(Object _bindingComponent, View _root,
      int _localFieldCount, ImageView cardviewImage, TextView contentTextview,
      TextView descriptionTv, View divider, View divider4, FloatingActionButton fab,
      Guideline guideline7, TextView textView11, TextView textView12, TextView uniNewsTitle,
      TextView usernameTextview) {
    super(_bindingComponent, _root, _localFieldCount);
    this.cardviewImage = cardviewImage;
    this.contentTextview = contentTextview;
    this.descriptionTv = descriptionTv;
    this.divider = divider;
    this.divider4 = divider4;
    this.fab = fab;
    this.guideline7 = guideline7;
    this.textView11 = textView11;
    this.textView12 = textView12;
    this.uniNewsTitle = uniNewsTitle;
    this.usernameTextview = usernameTextview;
  }

  public abstract void setUni(@Nullable University uni);

  @Nullable
  public University getUni() {
    return mUni;
  }

  public abstract void setCallback(@Nullable MainUniversityFragment callback);

  @Nullable
  public MainUniversityFragment getCallback() {
    return mCallback;
  }

  @NonNull
  public static FragmentMainUniversityBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_main_university, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentMainUniversityBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentMainUniversityBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_main_university, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentMainUniversityBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_main_university, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentMainUniversityBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentMainUniversityBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_main_university, null, false, component);
  }

  public static FragmentMainUniversityBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentMainUniversityBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentMainUniversityBinding)bind(component, view, com.example.uniguide2.R.layout.fragment_main_university);
  }
}
