package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.PostFragment;
import com.example.uniguide2.data.Post;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentPostBinding extends ViewDataBinding {
  @NonNull
  public final View divider4;

  @NonNull
  public final ImageView imageView3;

  @NonNull
  public final MaterialButton postButton;

  @NonNull
  public final TextInputEditText postEditText;

  @NonNull
  public final TextView postUsernameTextView;

  @NonNull
  public final TextInputLayout textInputLayout;

  @Bindable
  protected Post mPost;

  @Bindable
  protected PostFragment mCallback;

  protected FragmentPostBinding(Object _bindingComponent, View _root, int _localFieldCount,
      View divider4, ImageView imageView3, MaterialButton postButton,
      TextInputEditText postEditText, TextView postUsernameTextView,
      TextInputLayout textInputLayout) {
    super(_bindingComponent, _root, _localFieldCount);
    this.divider4 = divider4;
    this.imageView3 = imageView3;
    this.postButton = postButton;
    this.postEditText = postEditText;
    this.postUsernameTextView = postUsernameTextView;
    this.textInputLayout = textInputLayout;
  }

  public abstract void setPost(@Nullable Post post);

  @Nullable
  public Post getPost() {
    return mPost;
  }

  public abstract void setCallback(@Nullable PostFragment callback);

  @Nullable
  public PostFragment getCallback() {
    return mCallback;
  }

  @NonNull
  public static FragmentPostBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_post, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentPostBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentPostBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_post, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentPostBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_post, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentPostBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentPostBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_post, null, false, component);
  }

  public static FragmentPostBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentPostBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentPostBinding)bind(component, view, com.example.uniguide2.R.layout.fragment_post);
  }
}
