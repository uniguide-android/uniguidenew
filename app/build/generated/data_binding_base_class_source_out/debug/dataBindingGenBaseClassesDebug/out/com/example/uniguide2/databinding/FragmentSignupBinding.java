package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.SignUpFragment;
import com.example.uniguide2.data.Student;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentSignupBinding extends ViewDataBinding {
  @NonNull
  public final EditText aboutEditText;

  @NonNull
  public final TextInputEditText emailEditText;

  @NonNull
  public final TextInputLayout emailTextInput;

  @NonNull
  public final EditText gradeEditText;

  @NonNull
  public final TextInputEditText nameEditText;

  @NonNull
  public final TextInputLayout nameTextInput;

  @NonNull
  public final TextInputLayout passwordTextInput;

  @NonNull
  public final EditText schoolEditText;

  @NonNull
  public final MaterialButton signupButton;

  @NonNull
  public final TextInputEditText signupPasswordEditText;

  @NonNull
  public final TextInputEditText signupUsernameEditText;

  @NonNull
  public final TextView textView3;

  @NonNull
  public final TextView textView4;

  @NonNull
  public final TextView textView5;

  @NonNull
  public final TextView textView6;

  @NonNull
  public final TextInputLayout usernameTextInput;

  @Bindable
  protected Student mStud;

  @Bindable
  protected SignUpFragment mCallback;

  protected FragmentSignupBinding(Object _bindingComponent, View _root, int _localFieldCount,
      EditText aboutEditText, TextInputEditText emailEditText, TextInputLayout emailTextInput,
      EditText gradeEditText, TextInputEditText nameEditText, TextInputLayout nameTextInput,
      TextInputLayout passwordTextInput, EditText schoolEditText, MaterialButton signupButton,
      TextInputEditText signupPasswordEditText, TextInputEditText signupUsernameEditText,
      TextView textView3, TextView textView4, TextView textView5, TextView textView6,
      TextInputLayout usernameTextInput) {
    super(_bindingComponent, _root, _localFieldCount);
    this.aboutEditText = aboutEditText;
    this.emailEditText = emailEditText;
    this.emailTextInput = emailTextInput;
    this.gradeEditText = gradeEditText;
    this.nameEditText = nameEditText;
    this.nameTextInput = nameTextInput;
    this.passwordTextInput = passwordTextInput;
    this.schoolEditText = schoolEditText;
    this.signupButton = signupButton;
    this.signupPasswordEditText = signupPasswordEditText;
    this.signupUsernameEditText = signupUsernameEditText;
    this.textView3 = textView3;
    this.textView4 = textView4;
    this.textView5 = textView5;
    this.textView6 = textView6;
    this.usernameTextInput = usernameTextInput;
  }

  public abstract void setStud(@Nullable Student stud);

  @Nullable
  public Student getStud() {
    return mStud;
  }

  public abstract void setCallback(@Nullable SignUpFragment callback);

  @Nullable
  public SignUpFragment getCallback() {
    return mCallback;
  }

  @NonNull
  public static FragmentSignupBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_signup, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentSignupBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentSignupBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_signup, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentSignupBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_signup, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentSignupBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentSignupBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_signup, null, false, component);
  }

  public static FragmentSignupBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentSignupBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentSignupBinding)bind(component, view, com.example.uniguide2.R.layout.fragment_signup);
  }
}
