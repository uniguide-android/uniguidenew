package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.example.uniguide2.StudentsMainFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentStudentsMainBinding extends ViewDataBinding {
  @NonNull
  public final FloatingActionButton newPostButton;

  @NonNull
  public final RecyclerView recyclerView;

  @Bindable
  protected StudentsMainFragment mCallback;

  protected FragmentStudentsMainBinding(Object _bindingComponent, View _root, int _localFieldCount,
      FloatingActionButton newPostButton, RecyclerView recyclerView) {
    super(_bindingComponent, _root, _localFieldCount);
    this.newPostButton = newPostButton;
    this.recyclerView = recyclerView;
  }

  public abstract void setCallback(@Nullable StudentsMainFragment callback);

  @Nullable
  public StudentsMainFragment getCallback() {
    return mCallback;
  }

  @NonNull
  public static FragmentStudentsMainBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_students_main, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentStudentsMainBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentStudentsMainBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_students_main, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentStudentsMainBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_students_main, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentStudentsMainBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentStudentsMainBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_students_main, null, false, component);
  }

  public static FragmentStudentsMainBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentStudentsMainBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentStudentsMainBinding)bind(component, view, com.example.uniguide2.R.layout.fragment_students_main);
  }
}
