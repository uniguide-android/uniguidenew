package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.UniversitySignUpFragment;
import com.example.uniguide2.data.University;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentUniversitySignUpBinding extends ViewDataBinding {
  @NonNull
  public final TextInputEditText NameEt;

  @NonNull
  public final TextInputEditText addressEt;

  @NonNull
  public final TextInputEditText confirmPasswordEt;

  @NonNull
  public final TextInputEditText descriptionEt;

  @NonNull
  public final Guideline guideline3;

  @NonNull
  public final Guideline guideline4;

  @NonNull
  public final Guideline guideline5;

  @NonNull
  public final MaterialButton loginButton;

  @NonNull
  public final TextInputEditText passwordEt;

  @NonNull
  public final TextInputEditText phoneNumberEt;

  @NonNull
  public final TextView signUpTv;

  @NonNull
  public final TextInputLayout textInputLayout10;

  @NonNull
  public final TextInputLayout textInputLayout11;

  @NonNull
  public final TextInputLayout textInputLayout2;

  @NonNull
  public final TextInputLayout textInputLayout3;

  @NonNull
  public final TextInputLayout textInputLayout4;

  @NonNull
  public final TextInputLayout textInputLayout5;

  @NonNull
  public final TextInputLayout textInputLayout9;

  @NonNull
  public final TextInputEditText userNameEt;

  @Bindable
  protected University mUni;

  @Bindable
  protected UniversitySignUpFragment mCallback;

  protected FragmentUniversitySignUpBinding(Object _bindingComponent, View _root,
      int _localFieldCount, TextInputEditText NameEt, TextInputEditText addressEt,
      TextInputEditText confirmPasswordEt, TextInputEditText descriptionEt, Guideline guideline3,
      Guideline guideline4, Guideline guideline5, MaterialButton loginButton,
      TextInputEditText passwordEt, TextInputEditText phoneNumberEt, TextView signUpTv,
      TextInputLayout textInputLayout10, TextInputLayout textInputLayout11,
      TextInputLayout textInputLayout2, TextInputLayout textInputLayout3,
      TextInputLayout textInputLayout4, TextInputLayout textInputLayout5,
      TextInputLayout textInputLayout9, TextInputEditText userNameEt) {
    super(_bindingComponent, _root, _localFieldCount);
    this.NameEt = NameEt;
    this.addressEt = addressEt;
    this.confirmPasswordEt = confirmPasswordEt;
    this.descriptionEt = descriptionEt;
    this.guideline3 = guideline3;
    this.guideline4 = guideline4;
    this.guideline5 = guideline5;
    this.loginButton = loginButton;
    this.passwordEt = passwordEt;
    this.phoneNumberEt = phoneNumberEt;
    this.signUpTv = signUpTv;
    this.textInputLayout10 = textInputLayout10;
    this.textInputLayout11 = textInputLayout11;
    this.textInputLayout2 = textInputLayout2;
    this.textInputLayout3 = textInputLayout3;
    this.textInputLayout4 = textInputLayout4;
    this.textInputLayout5 = textInputLayout5;
    this.textInputLayout9 = textInputLayout9;
    this.userNameEt = userNameEt;
  }

  public abstract void setUni(@Nullable University uni);

  @Nullable
  public University getUni() {
    return mUni;
  }

  public abstract void setCallback(@Nullable UniversitySignUpFragment callback);

  @Nullable
  public UniversitySignUpFragment getCallback() {
    return mCallback;
  }

  @NonNull
  public static FragmentUniversitySignUpBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_university_sign_up, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentUniversitySignUpBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentUniversitySignUpBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_university_sign_up, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentUniversitySignUpBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_university_sign_up, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentUniversitySignUpBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentUniversitySignUpBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.fragment_university_sign_up, null, false, component);
  }

  public static FragmentUniversitySignUpBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentUniversitySignUpBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (FragmentUniversitySignUpBinding)bind(component, view, com.example.uniguide2.R.layout.fragment_university_sign_up);
  }
}
