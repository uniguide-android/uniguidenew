package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.data.Career;
import com.example.uniguide2.eventHandler.CareerEventListener;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class LayoutCareerItemBinding extends ViewDataBinding {
  @NonNull
  public final TextView careerName;

  @Bindable
  protected Career mGroup;

  @Bindable
  protected CareerEventListener mItemClickListener;

  protected LayoutCareerItemBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextView careerName) {
    super(_bindingComponent, _root, _localFieldCount);
    this.careerName = careerName;
  }

  public abstract void setGroup(@Nullable Career group);

  @Nullable
  public Career getGroup() {
    return mGroup;
  }

  public abstract void setItemClickListener(@Nullable CareerEventListener itemClickListener);

  @Nullable
  public CareerEventListener getItemClickListener() {
    return mItemClickListener;
  }

  @NonNull
  public static LayoutCareerItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.layout_career_item, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static LayoutCareerItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<LayoutCareerItemBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.layout_career_item, root, attachToRoot, component);
  }

  @NonNull
  public static LayoutCareerItemBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.layout_career_item, null, false, component)
   */
  @NonNull
  @Deprecated
  public static LayoutCareerItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<LayoutCareerItemBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.layout_career_item, null, false, component);
  }

  public static LayoutCareerItemBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static LayoutCareerItemBinding bind(@NonNull View view, @Nullable Object component) {
    return (LayoutCareerItemBinding)bind(component, view, com.example.uniguide2.R.layout.layout_career_item);
  }
}
