package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.data.Post;
import com.google.android.material.button.MaterialButton;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class RecyclerViewItemBinding extends ViewDataBinding {
  @NonNull
  public final ImageView cardviewImage;

  @NonNull
  public final ConstraintLayout constraintLayout;

  @NonNull
  public final TextView contentTextview;

  @NonNull
  public final TextView dateTextview;

  @NonNull
  public final View divider;

  @NonNull
  public final View divider2;

  @NonNull
  public final Guideline guideline2;

  @NonNull
  public final MaterialButton likeButton;

  @NonNull
  public final MaterialButton reportButton;

  @NonNull
  public final TextView usernameTextview;

  @Bindable
  protected Post mPost;

  protected RecyclerViewItemBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ImageView cardviewImage, ConstraintLayout constraintLayout, TextView contentTextview,
      TextView dateTextview, View divider, View divider2, Guideline guideline2,
      MaterialButton likeButton, MaterialButton reportButton, TextView usernameTextview) {
    super(_bindingComponent, _root, _localFieldCount);
    this.cardviewImage = cardviewImage;
    this.constraintLayout = constraintLayout;
    this.contentTextview = contentTextview;
    this.dateTextview = dateTextview;
    this.divider = divider;
    this.divider2 = divider2;
    this.guideline2 = guideline2;
    this.likeButton = likeButton;
    this.reportButton = reportButton;
    this.usernameTextview = usernameTextview;
  }

  public abstract void setPost(@Nullable Post post);

  @Nullable
  public Post getPost() {
    return mPost;
  }

  @NonNull
  public static RecyclerViewItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.recycler_view_item, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static RecyclerViewItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<RecyclerViewItemBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.recycler_view_item, root, attachToRoot, component);
  }

  @NonNull
  public static RecyclerViewItemBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.recycler_view_item, null, false, component)
   */
  @NonNull
  @Deprecated
  public static RecyclerViewItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<RecyclerViewItemBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.recycler_view_item, null, false, component);
  }

  public static RecyclerViewItemBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static RecyclerViewItemBinding bind(@NonNull View view, @Nullable Object component) {
    return (RecyclerViewItemBinding)bind(component, view, com.example.uniguide2.R.layout.recycler_view_item);
  }
}
