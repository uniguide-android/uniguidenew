package com.example.uniguide2.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.data.News;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class RecyclerViewNewsItemBinding extends ViewDataBinding {
  @NonNull
  public final TextView authorTextview;

  @NonNull
  public final ConstraintLayout constraintLayout;

  @NonNull
  public final View divider;

  @NonNull
  public final View divider2;

  @NonNull
  public final TextView newsContentTextview;

  @NonNull
  public final TextView titleTextview;

  @Bindable
  protected News mNews;

  protected RecyclerViewNewsItemBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextView authorTextview, ConstraintLayout constraintLayout, View divider, View divider2,
      TextView newsContentTextview, TextView titleTextview) {
    super(_bindingComponent, _root, _localFieldCount);
    this.authorTextview = authorTextview;
    this.constraintLayout = constraintLayout;
    this.divider = divider;
    this.divider2 = divider2;
    this.newsContentTextview = newsContentTextview;
    this.titleTextview = titleTextview;
  }

  public abstract void setNews(@Nullable News news);

  @Nullable
  public News getNews() {
    return mNews;
  }

  @NonNull
  public static RecyclerViewNewsItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.recycler_view_news_item, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static RecyclerViewNewsItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<RecyclerViewNewsItemBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.recycler_view_news_item, root, attachToRoot, component);
  }

  @NonNull
  public static RecyclerViewNewsItemBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.recycler_view_news_item, null, false, component)
   */
  @NonNull
  @Deprecated
  public static RecyclerViewNewsItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<RecyclerViewNewsItemBinding>inflateInternal(inflater, com.example.uniguide2.R.layout.recycler_view_news_item, null, false, component);
  }

  public static RecyclerViewNewsItemBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static RecyclerViewNewsItemBinding bind(@NonNull View view, @Nullable Object component) {
    return (RecyclerViewNewsItemBinding)bind(component, view, com.example.uniguide2.R.layout.recycler_view_news_item);
  }
}
