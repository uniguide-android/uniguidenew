package com.example.uniguide2;

public class BR {
  public static final int _all = 0;

  public static final int news = 1;

  public static final int uni = 2;

  public static final int agent = 3;

  public static final int post = 4;

  public static final int question = 5;

  public static final int stud = 6;

  public static final int profile = 7;

  public static final int callback = 8;

  public static final int user = 9;

  public static final int group = 10;

  public static final int itemClickListener = 11;
}
