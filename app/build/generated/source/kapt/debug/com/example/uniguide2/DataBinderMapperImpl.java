package com.example.uniguide2;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.example.uniguide2.databinding.FragmentAgentPostBindingImpl;
import com.example.uniguide2.databinding.FragmentAgentSignUpBindingImpl;
import com.example.uniguide2.databinding.FragmentCareerBindingImpl;
import com.example.uniguide2.databinding.FragmentCareerDetailBindingImpl;
import com.example.uniguide2.databinding.FragmentCareerViewBindingImpl;
import com.example.uniguide2.databinding.FragmentLoginBindingImpl;
import com.example.uniguide2.databinding.FragmentMainAgentBindingImpl;
import com.example.uniguide2.databinding.FragmentMainUniversityBindingImpl;
import com.example.uniguide2.databinding.FragmentNewsBindingImpl;
import com.example.uniguide2.databinding.FragmentPostBindingImpl;
import com.example.uniguide2.databinding.FragmentQuestionBindingImpl;
import com.example.uniguide2.databinding.FragmentSignupBindingImpl;
import com.example.uniguide2.databinding.FragmentStudentsMainBindingImpl;
import com.example.uniguide2.databinding.FragmentUniPostBindingImpl;
import com.example.uniguide2.databinding.FragmentUniversitySignUpBindingImpl;
import com.example.uniguide2.databinding.LayoutCareerItemBindingImpl;
import com.example.uniguide2.databinding.RecyclerViewItemBindingImpl;
import com.example.uniguide2.databinding.RecyclerViewNewsItemBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_FRAGMENTAGENTPOST = 1;

  private static final int LAYOUT_FRAGMENTAGENTSIGNUP = 2;

  private static final int LAYOUT_FRAGMENTCAREER = 3;

  private static final int LAYOUT_FRAGMENTCAREERDETAIL = 4;

  private static final int LAYOUT_FRAGMENTCAREERVIEW = 5;

  private static final int LAYOUT_FRAGMENTLOGIN = 6;

  private static final int LAYOUT_FRAGMENTMAINAGENT = 7;

  private static final int LAYOUT_FRAGMENTMAINUNIVERSITY = 8;

  private static final int LAYOUT_FRAGMENTNEWS = 9;

  private static final int LAYOUT_FRAGMENTPOST = 10;

  private static final int LAYOUT_FRAGMENTQUESTION = 11;

  private static final int LAYOUT_FRAGMENTSIGNUP = 12;

  private static final int LAYOUT_FRAGMENTSTUDENTSMAIN = 13;

  private static final int LAYOUT_FRAGMENTUNIPOST = 14;

  private static final int LAYOUT_FRAGMENTUNIVERSITYSIGNUP = 15;

  private static final int LAYOUT_LAYOUTCAREERITEM = 16;

  private static final int LAYOUT_RECYCLERVIEWITEM = 17;

  private static final int LAYOUT_RECYCLERVIEWNEWSITEM = 18;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(18);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_agent_post, LAYOUT_FRAGMENTAGENTPOST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_agent_sign_up, LAYOUT_FRAGMENTAGENTSIGNUP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_career, LAYOUT_FRAGMENTCAREER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_career_detail, LAYOUT_FRAGMENTCAREERDETAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_career_view, LAYOUT_FRAGMENTCAREERVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_login, LAYOUT_FRAGMENTLOGIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_main_agent, LAYOUT_FRAGMENTMAINAGENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_main_university, LAYOUT_FRAGMENTMAINUNIVERSITY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_news, LAYOUT_FRAGMENTNEWS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_post, LAYOUT_FRAGMENTPOST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_question, LAYOUT_FRAGMENTQUESTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_signup, LAYOUT_FRAGMENTSIGNUP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_students_main, LAYOUT_FRAGMENTSTUDENTSMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_uni_post, LAYOUT_FRAGMENTUNIPOST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.fragment_university_sign_up, LAYOUT_FRAGMENTUNIVERSITYSIGNUP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.layout_career_item, LAYOUT_LAYOUTCAREERITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.recycler_view_item, LAYOUT_RECYCLERVIEWITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.uniguide2.R.layout.recycler_view_news_item, LAYOUT_RECYCLERVIEWNEWSITEM);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_FRAGMENTAGENTPOST: {
          if ("layout/fragment_agent_post_0".equals(tag)) {
            return new FragmentAgentPostBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_agent_post is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTAGENTSIGNUP: {
          if ("layout/fragment_agent_sign_up_0".equals(tag)) {
            return new FragmentAgentSignUpBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_agent_sign_up is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTCAREER: {
          if ("layout/fragment_career_0".equals(tag)) {
            return new FragmentCareerBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_career is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTCAREERDETAIL: {
          if ("layout/fragment_career_detail_0".equals(tag)) {
            return new FragmentCareerDetailBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_career_detail is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTCAREERVIEW: {
          if ("layout/fragment_career_view_0".equals(tag)) {
            return new FragmentCareerViewBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_career_view is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTLOGIN: {
          if ("layout/fragment_login_0".equals(tag)) {
            return new FragmentLoginBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_login is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTMAINAGENT: {
          if ("layout/fragment_main_agent_0".equals(tag)) {
            return new FragmentMainAgentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_main_agent is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTMAINUNIVERSITY: {
          if ("layout/fragment_main_university_0".equals(tag)) {
            return new FragmentMainUniversityBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_main_university is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTNEWS: {
          if ("layout/fragment_news_0".equals(tag)) {
            return new FragmentNewsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_news is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTPOST: {
          if ("layout/fragment_post_0".equals(tag)) {
            return new FragmentPostBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_post is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTQUESTION: {
          if ("layout/fragment_question_0".equals(tag)) {
            return new FragmentQuestionBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_question is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSIGNUP: {
          if ("layout/fragment_signup_0".equals(tag)) {
            return new FragmentSignupBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_signup is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSTUDENTSMAIN: {
          if ("layout/fragment_students_main_0".equals(tag)) {
            return new FragmentStudentsMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_students_main is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTUNIPOST: {
          if ("layout/fragment_uni_post_0".equals(tag)) {
            return new FragmentUniPostBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_uni_post is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTUNIVERSITYSIGNUP: {
          if ("layout/fragment_university_sign_up_0".equals(tag)) {
            return new FragmentUniversitySignUpBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_university_sign_up is invalid. Received: " + tag);
        }
        case  LAYOUT_LAYOUTCAREERITEM: {
          if ("layout/layout_career_item_0".equals(tag)) {
            return new LayoutCareerItemBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for layout_career_item is invalid. Received: " + tag);
        }
        case  LAYOUT_RECYCLERVIEWITEM: {
          if ("layout/recycler_view_item_0".equals(tag)) {
            return new RecyclerViewItemBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for recycler_view_item is invalid. Received: " + tag);
        }
        case  LAYOUT_RECYCLERVIEWNEWSITEM: {
          if ("layout/recycler_view_news_item_0".equals(tag)) {
            return new RecyclerViewNewsItemBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for recycler_view_news_item is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(13);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "news");
      sKeys.put(2, "uni");
      sKeys.put(3, "agent");
      sKeys.put(4, "post");
      sKeys.put(5, "question");
      sKeys.put(6, "stud");
      sKeys.put(7, "profile");
      sKeys.put(8, "callback");
      sKeys.put(9, "user");
      sKeys.put(10, "group");
      sKeys.put(11, "itemClickListener");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(18);

    static {
      sKeys.put("layout/fragment_agent_post_0", com.example.uniguide2.R.layout.fragment_agent_post);
      sKeys.put("layout/fragment_agent_sign_up_0", com.example.uniguide2.R.layout.fragment_agent_sign_up);
      sKeys.put("layout/fragment_career_0", com.example.uniguide2.R.layout.fragment_career);
      sKeys.put("layout/fragment_career_detail_0", com.example.uniguide2.R.layout.fragment_career_detail);
      sKeys.put("layout/fragment_career_view_0", com.example.uniguide2.R.layout.fragment_career_view);
      sKeys.put("layout/fragment_login_0", com.example.uniguide2.R.layout.fragment_login);
      sKeys.put("layout/fragment_main_agent_0", com.example.uniguide2.R.layout.fragment_main_agent);
      sKeys.put("layout/fragment_main_university_0", com.example.uniguide2.R.layout.fragment_main_university);
      sKeys.put("layout/fragment_news_0", com.example.uniguide2.R.layout.fragment_news);
      sKeys.put("layout/fragment_post_0", com.example.uniguide2.R.layout.fragment_post);
      sKeys.put("layout/fragment_question_0", com.example.uniguide2.R.layout.fragment_question);
      sKeys.put("layout/fragment_signup_0", com.example.uniguide2.R.layout.fragment_signup);
      sKeys.put("layout/fragment_students_main_0", com.example.uniguide2.R.layout.fragment_students_main);
      sKeys.put("layout/fragment_uni_post_0", com.example.uniguide2.R.layout.fragment_uni_post);
      sKeys.put("layout/fragment_university_sign_up_0", com.example.uniguide2.R.layout.fragment_university_sign_up);
      sKeys.put("layout/layout_career_item_0", com.example.uniguide2.R.layout.layout_career_item);
      sKeys.put("layout/recycler_view_item_0", com.example.uniguide2.R.layout.recycler_view_item);
      sKeys.put("layout/recycler_view_news_item_0", com.example.uniguide2.R.layout.recycler_view_news_item);
    }
  }
}
