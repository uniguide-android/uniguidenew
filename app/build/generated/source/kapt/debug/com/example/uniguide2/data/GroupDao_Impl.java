package com.example.uniguide2.data;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings({"unchecked", "deprecation"})
public final class GroupDao_Impl implements GroupDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfCareer;

  public GroupDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfCareer = new EntityInsertionAdapter<Career>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `career_groups`(`group_code`,`group_name`,`group_jobs`,`group_description`) VALUES (?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Career value) {
        if (value.getGroupCode() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getGroupCode());
        }
        if (value.getGroupName() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getGroupName());
        }
        if (value.getOccupations() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getOccupations());
        }
        if (value.getDescription() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getDescription());
        }
      }
    };
  }

  @Override
  public long insertGroup(final Career group) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfCareer.insertAndReturnId(group);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<Career> getCareerGroupByCode(final String code) {
    final String _sql = "Select * from career_groups where group_code= ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (code == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, code);
    }
    return __db.getInvalidationTracker().createLiveData(new String[]{"career_groups"}, false, new Callable<Career>() {
      @Override
      public Career call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfGroupCode = CursorUtil.getColumnIndexOrThrow(_cursor, "group_code");
          final int _cursorIndexOfGroupName = CursorUtil.getColumnIndexOrThrow(_cursor, "group_name");
          final int _cursorIndexOfOccupations = CursorUtil.getColumnIndexOrThrow(_cursor, "group_jobs");
          final int _cursorIndexOfDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "group_description");
          final Career _result;
          if(_cursor.moveToFirst()) {
            final String _tmpGroupCode;
            _tmpGroupCode = _cursor.getString(_cursorIndexOfGroupCode);
            final String _tmpGroupName;
            _tmpGroupName = _cursor.getString(_cursorIndexOfGroupName);
            final String _tmpOccupations;
            _tmpOccupations = _cursor.getString(_cursorIndexOfOccupations);
            final String _tmpDescription;
            _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
            _result = new Career(_tmpGroupCode,_tmpGroupName,_tmpOccupations,_tmpDescription);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<List<Career>> getAllGroups() {
    final String _sql = "Select * from career_groups";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"career_groups"}, false, new Callable<List<Career>>() {
      @Override
      public List<Career> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfGroupCode = CursorUtil.getColumnIndexOrThrow(_cursor, "group_code");
          final int _cursorIndexOfGroupName = CursorUtil.getColumnIndexOrThrow(_cursor, "group_name");
          final int _cursorIndexOfOccupations = CursorUtil.getColumnIndexOrThrow(_cursor, "group_jobs");
          final int _cursorIndexOfDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "group_description");
          final List<Career> _result = new ArrayList<Career>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Career _item;
            final String _tmpGroupCode;
            _tmpGroupCode = _cursor.getString(_cursorIndexOfGroupCode);
            final String _tmpGroupName;
            _tmpGroupName = _cursor.getString(_cursorIndexOfGroupName);
            final String _tmpOccupations;
            _tmpOccupations = _cursor.getString(_cursorIndexOfOccupations);
            final String _tmpDescription;
            _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
            _item = new Career(_tmpGroupCode,_tmpGroupName,_tmpOccupations,_tmpDescription);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }
}
