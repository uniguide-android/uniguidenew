package com.example.uniguide2.data;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings({"unchecked", "deprecation"})
public final class QuestionDao_Impl implements QuestionDao {
  private final RoomDatabase __db;

  private final SharedSQLiteStatement __preparedStmtOfInsertQuestion;

  private final SharedSQLiteStatement __preparedStmtOfDeleteQuestion;

  private final SharedSQLiteStatement __preparedStmtOfUpdateGroup;

  public QuestionDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__preparedStmtOfInsertQuestion = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "Insert into career_questions VALUES (?, ?) ";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteQuestion = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "Delete from career_questions where question_description= ?";
        return _query;
      }
    };
    this.__preparedStmtOfUpdateGroup = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "Update career_questions SET career_code = ? where question_description= ?";
        return _query;
      }
    };
  }

  @Override
  public void insertQuestion(final String code, final String question) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfInsertQuestion.acquire();
    int _argIndex = 1;
    if (code == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, code);
    }
    _argIndex = 2;
    if (question == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, question);
    }
    __db.beginTransaction();
    try {
      _stmt.executeInsert();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfInsertQuestion.release(_stmt);
    }
  }

  @Override
  public void deleteQuestion(final String questionDescr) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteQuestion.acquire();
    int _argIndex = 1;
    if (questionDescr == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, questionDescr);
    }
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteQuestion.release(_stmt);
    }
  }

  @Override
  public void updateGroup(final String code, final String questionDescr) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdateGroup.acquire();
    int _argIndex = 1;
    if (code == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, code);
    }
    _argIndex = 2;
    if (questionDescr == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, questionDescr);
    }
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdateGroup.release(_stmt);
    }
  }

  @Override
  public LiveData<CareerQuestion> getQuestionByDesc(final String questionDescr) {
    final String _sql = "Select * from career_questions where question_description= ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (questionDescr == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, questionDescr);
    }
    return __db.getInvalidationTracker().createLiveData(new String[]{"career_questions"}, false, new Callable<CareerQuestion>() {
      @Override
      public CareerQuestion call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfCareerQuestion = CursorUtil.getColumnIndexOrThrow(_cursor, "question_description");
          final int _cursorIndexOfCareerCode = CursorUtil.getColumnIndexOrThrow(_cursor, "career_code");
          final CareerQuestion _result;
          if(_cursor.moveToFirst()) {
            final String _tmpCareerQuestion;
            _tmpCareerQuestion = _cursor.getString(_cursorIndexOfCareerQuestion);
            final String _tmpCareerCode;
            _tmpCareerCode = _cursor.getString(_cursorIndexOfCareerCode);
            _result = new CareerQuestion(_tmpCareerQuestion,_tmpCareerCode);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<List<CareerQuestion>> getAllQuestions() {
    final String _sql = "Select * from career_questions";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"career_questions"}, false, new Callable<List<CareerQuestion>>() {
      @Override
      public List<CareerQuestion> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfCareerQuestion = CursorUtil.getColumnIndexOrThrow(_cursor, "question_description");
          final int _cursorIndexOfCareerCode = CursorUtil.getColumnIndexOrThrow(_cursor, "career_code");
          final List<CareerQuestion> _result = new ArrayList<CareerQuestion>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final CareerQuestion _item;
            final String _tmpCareerQuestion;
            _tmpCareerQuestion = _cursor.getString(_cursorIndexOfCareerQuestion);
            final String _tmpCareerCode;
            _tmpCareerCode = _cursor.getString(_cursorIndexOfCareerCode);
            _item = new CareerQuestion(_tmpCareerQuestion,_tmpCareerCode);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }
}
