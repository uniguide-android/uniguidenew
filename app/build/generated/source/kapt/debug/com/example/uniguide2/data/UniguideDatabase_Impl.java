package com.example.uniguide2.data;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class UniguideDatabase_Impl extends UniguideDatabase {
  private volatile GroupDao _groupDao;

  private volatile QuestionDao _questionDao;

  private volatile PostDao _postDao;

  private volatile NewsDao _newsDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(3) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `career_groups` (`group_code` TEXT NOT NULL, `group_name` TEXT NOT NULL, `group_jobs` TEXT NOT NULL, `group_description` TEXT NOT NULL, PRIMARY KEY(`group_code`))");
        _db.execSQL("CREATE  INDEX `index_career_groups_group_code` ON `career_groups` (`group_code`)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `career_questions` (`question_description` TEXT NOT NULL, `career_code` TEXT NOT NULL, PRIMARY KEY(`question_description`), FOREIGN KEY(`career_code`) REFERENCES `career_groups`(`group_code`) ON UPDATE NO ACTION ON DELETE NO ACTION )");
        _db.execSQL("CREATE  INDEX `index_career_questions_career_code` ON `career_questions` (`career_code`)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `Post` (`id` INTEGER NOT NULL, `content` TEXT NOT NULL, `type` TEXT NOT NULL, `flag` TEXT NOT NULL, `likes` INTEGER NOT NULL, `username` TEXT NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `News` (`id` INTEGER NOT NULL, `title` TEXT NOT NULL, `content` TEXT NOT NULL, `author` TEXT NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"4325f859575a1c14511b709ea7c3d609\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `career_groups`");
        _db.execSQL("DROP TABLE IF EXISTS `career_questions`");
        _db.execSQL("DROP TABLE IF EXISTS `Post`");
        _db.execSQL("DROP TABLE IF EXISTS `News`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        _db.execSQL("PRAGMA foreign_keys = ON");
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsCareerGroups = new HashMap<String, TableInfo.Column>(4);
        _columnsCareerGroups.put("group_code", new TableInfo.Column("group_code", "TEXT", true, 1));
        _columnsCareerGroups.put("group_name", new TableInfo.Column("group_name", "TEXT", true, 0));
        _columnsCareerGroups.put("group_jobs", new TableInfo.Column("group_jobs", "TEXT", true, 0));
        _columnsCareerGroups.put("group_description", new TableInfo.Column("group_description", "TEXT", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysCareerGroups = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesCareerGroups = new HashSet<TableInfo.Index>(1);
        _indicesCareerGroups.add(new TableInfo.Index("index_career_groups_group_code", false, Arrays.asList("group_code")));
        final TableInfo _infoCareerGroups = new TableInfo("career_groups", _columnsCareerGroups, _foreignKeysCareerGroups, _indicesCareerGroups);
        final TableInfo _existingCareerGroups = TableInfo.read(_db, "career_groups");
        if (! _infoCareerGroups.equals(_existingCareerGroups)) {
          throw new IllegalStateException("Migration didn't properly handle career_groups(com.example.uniguide2.data.Career).\n"
                  + " Expected:\n" + _infoCareerGroups + "\n"
                  + " Found:\n" + _existingCareerGroups);
        }
        final HashMap<String, TableInfo.Column> _columnsCareerQuestions = new HashMap<String, TableInfo.Column>(2);
        _columnsCareerQuestions.put("question_description", new TableInfo.Column("question_description", "TEXT", true, 1));
        _columnsCareerQuestions.put("career_code", new TableInfo.Column("career_code", "TEXT", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysCareerQuestions = new HashSet<TableInfo.ForeignKey>(1);
        _foreignKeysCareerQuestions.add(new TableInfo.ForeignKey("career_groups", "NO ACTION", "NO ACTION",Arrays.asList("career_code"), Arrays.asList("group_code")));
        final HashSet<TableInfo.Index> _indicesCareerQuestions = new HashSet<TableInfo.Index>(1);
        _indicesCareerQuestions.add(new TableInfo.Index("index_career_questions_career_code", false, Arrays.asList("career_code")));
        final TableInfo _infoCareerQuestions = new TableInfo("career_questions", _columnsCareerQuestions, _foreignKeysCareerQuestions, _indicesCareerQuestions);
        final TableInfo _existingCareerQuestions = TableInfo.read(_db, "career_questions");
        if (! _infoCareerQuestions.equals(_existingCareerQuestions)) {
          throw new IllegalStateException("Migration didn't properly handle career_questions(com.example.uniguide2.data.CareerQuestion).\n"
                  + " Expected:\n" + _infoCareerQuestions + "\n"
                  + " Found:\n" + _existingCareerQuestions);
        }
        final HashMap<String, TableInfo.Column> _columnsPost = new HashMap<String, TableInfo.Column>(6);
        _columnsPost.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsPost.put("content", new TableInfo.Column("content", "TEXT", true, 0));
        _columnsPost.put("type", new TableInfo.Column("type", "TEXT", true, 0));
        _columnsPost.put("flag", new TableInfo.Column("flag", "TEXT", true, 0));
        _columnsPost.put("likes", new TableInfo.Column("likes", "INTEGER", true, 0));
        _columnsPost.put("username", new TableInfo.Column("username", "TEXT", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysPost = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesPost = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoPost = new TableInfo("Post", _columnsPost, _foreignKeysPost, _indicesPost);
        final TableInfo _existingPost = TableInfo.read(_db, "Post");
        if (! _infoPost.equals(_existingPost)) {
          throw new IllegalStateException("Migration didn't properly handle Post(com.example.uniguide2.data.Post).\n"
                  + " Expected:\n" + _infoPost + "\n"
                  + " Found:\n" + _existingPost);
        }
        final HashMap<String, TableInfo.Column> _columnsNews = new HashMap<String, TableInfo.Column>(4);
        _columnsNews.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsNews.put("title", new TableInfo.Column("title", "TEXT", true, 0));
        _columnsNews.put("content", new TableInfo.Column("content", "TEXT", true, 0));
        _columnsNews.put("author", new TableInfo.Column("author", "TEXT", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysNews = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesNews = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoNews = new TableInfo("News", _columnsNews, _foreignKeysNews, _indicesNews);
        final TableInfo _existingNews = TableInfo.read(_db, "News");
        if (! _infoNews.equals(_existingNews)) {
          throw new IllegalStateException("Migration didn't properly handle News(com.example.uniguide2.data.News).\n"
                  + " Expected:\n" + _infoNews + "\n"
                  + " Found:\n" + _existingNews);
        }
      }
    }, "4325f859575a1c14511b709ea7c3d609", "c8c7d3382a16f2302b5f1de3da3dcaa2");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "career_groups","career_questions","Post","News");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    boolean _supportsDeferForeignKeys = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP;
    try {
      if (!_supportsDeferForeignKeys) {
        _db.execSQL("PRAGMA foreign_keys = FALSE");
      }
      super.beginTransaction();
      if (_supportsDeferForeignKeys) {
        _db.execSQL("PRAGMA defer_foreign_keys = TRUE");
      }
      _db.execSQL("DELETE FROM `career_questions`");
      _db.execSQL("DELETE FROM `career_groups`");
      _db.execSQL("DELETE FROM `Post`");
      _db.execSQL("DELETE FROM `News`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      if (!_supportsDeferForeignKeys) {
        _db.execSQL("PRAGMA foreign_keys = TRUE");
      }
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public GroupDao groupDao() {
    if (_groupDao != null) {
      return _groupDao;
    } else {
      synchronized(this) {
        if(_groupDao == null) {
          _groupDao = new GroupDao_Impl(this);
        }
        return _groupDao;
      }
    }
  }

  @Override
  public QuestionDao questionDao() {
    if (_questionDao != null) {
      return _questionDao;
    } else {
      synchronized(this) {
        if(_questionDao == null) {
          _questionDao = new QuestionDao_Impl(this);
        }
        return _questionDao;
      }
    }
  }

  @Override
  public PostDao postDao() {
    if (_postDao != null) {
      return _postDao;
    } else {
      synchronized(this) {
        if(_postDao == null) {
          _postDao = new PostDao_Impl(this);
        }
        return _postDao;
      }
    }
  }

  @Override
  public NewsDao newsDao() {
    if (_newsDao != null) {
      return _newsDao;
    } else {
      synchronized(this) {
        if(_newsDao == null) {
          _newsDao = new NewsDao_Impl(this);
        }
        return _newsDao;
      }
    }
  }
}
