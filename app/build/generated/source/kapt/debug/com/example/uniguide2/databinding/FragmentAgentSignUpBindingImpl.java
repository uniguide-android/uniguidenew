package com.example.uniguide2.databinding;
import com.example.uniguide2.R;
import com.example.uniguide2.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentAgentSignUpBindingImpl extends FragmentAgentSignUpBinding implements com.example.uniguide2.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.signUp_tv, 9);
        sViewsWithIds.put(R.id.textInputLayout4, 10);
        sViewsWithIds.put(R.id.textInputLayout3, 11);
        sViewsWithIds.put(R.id.guideline3, 12);
        sViewsWithIds.put(R.id.guideline4, 13);
        sViewsWithIds.put(R.id.guideline5, 14);
        sViewsWithIds.put(R.id.textInputLayout5, 15);
        sViewsWithIds.put(R.id.confirmPassword_et, 16);
        sViewsWithIds.put(R.id.textInputLayout6, 17);
        sViewsWithIds.put(R.id.textInputLayout9, 18);
        sViewsWithIds.put(R.id.textInputLayout10, 19);
        sViewsWithIds.put(R.id.textInputLayout11, 20);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback5;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentAgentSignUpBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private FragmentAgentSignUpBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.textfield.TextInputEditText) bindings[1]
            , (com.google.android.material.textfield.TextInputEditText) bindings[6]
            , (com.google.android.material.textfield.TextInputEditText) bindings[16]
            , (com.google.android.material.textfield.TextInputEditText) bindings[7]
            , (com.google.android.material.textfield.TextInputEditText) bindings[4]
            , (androidx.constraintlayout.widget.Guideline) bindings[12]
            , (androidx.constraintlayout.widget.Guideline) bindings[13]
            , (androidx.constraintlayout.widget.Guideline) bindings[14]
            , (com.google.android.material.button.MaterialButton) bindings[8]
            , (com.google.android.material.textfield.TextInputEditText) bindings[3]
            , (com.google.android.material.textfield.TextInputEditText) bindings[5]
            , (android.widget.TextView) bindings[9]
            , (com.google.android.material.textfield.TextInputLayout) bindings[19]
            , (com.google.android.material.textfield.TextInputLayout) bindings[20]
            , (com.google.android.material.textfield.TextInputLayout) bindings[11]
            , (com.google.android.material.textfield.TextInputLayout) bindings[10]
            , (com.google.android.material.textfield.TextInputLayout) bindings[15]
            , (com.google.android.material.textfield.TextInputLayout) bindings[17]
            , (com.google.android.material.textfield.TextInputLayout) bindings[18]
            , (com.google.android.material.textfield.TextInputEditText) bindings[2]
            );
        this.NameEt.setTag(null);
        this.addressEt.setTag(null);
        this.descriptionEt.setTag(null);
        this.emailEt.setTag(null);
        this.loginButton.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.passwordEt.setTag(null);
        this.phoneNumberEt.setTag(null);
        this.userNameEt.setTag(null);
        setRootTag(root);
        // listeners
        mCallback5 = new com.example.uniguide2.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.agent == variableId) {
            setAgent((com.example.uniguide2.data.Agent) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((com.example.uniguide2.AgentSignUpFragment) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setAgent(@Nullable com.example.uniguide2.data.Agent Agent) {
        this.mAgent = Agent;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.agent);
        super.requestRebind();
    }
    public void setCallback(@Nullable com.example.uniguide2.AgentSignUpFragment Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String agentUserUsername = null;
        java.lang.String agentDescription = null;
        com.example.uniguide2.data.Agent agent = mAgent;
        com.example.uniguide2.AgentSignUpFragment callback = mCallback;
        java.lang.String agentName = null;
        java.lang.String agentAddress = null;
        java.lang.String agentPhoneNo = null;
        java.lang.String agentEmail = null;
        java.lang.String agentUserPassword = null;
        com.example.uniguide2.data.User agentUser = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (agent != null) {
                    // read agent.description
                    agentDescription = agent.getDescription();
                    // read agent.name
                    agentName = agent.getName();
                    // read agent.address
                    agentAddress = agent.getAddress();
                    // read agent.phoneNo
                    agentPhoneNo = agent.getPhoneNo();
                    // read agent.email
                    agentEmail = agent.getEmail();
                    // read agent.user
                    agentUser = agent.getUser();
                }


                if (agentUser != null) {
                    // read agent.user.username
                    agentUserUsername = agentUser.getUsername();
                    // read agent.user.password
                    agentUserPassword = agentUser.getPassword();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.NameEt, agentName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.addressEt, agentAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.descriptionEt, agentDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.emailEt, agentEmail);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.passwordEt, agentUserPassword);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.phoneNumberEt, agentPhoneNo);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.userNameEt, agentUserUsername);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.loginButton.setOnClickListener(mCallback5);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // callback
        com.example.uniguide2.AgentSignUpFragment callback = mCallback;
        // callback != null
        boolean callbackJavaLangObjectNull = false;



        callbackJavaLangObjectNull = (callback) != (null);
        if (callbackJavaLangObjectNull) {


            callback.signAgent();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): agent
        flag 1 (0x2L): callback
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}