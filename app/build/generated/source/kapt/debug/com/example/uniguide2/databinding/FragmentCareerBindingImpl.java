package com.example.uniguide2.databinding;
import com.example.uniguide2.R;
import com.example.uniguide2.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentCareerBindingImpl extends FragmentCareerBinding implements com.example.uniguide2.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView3, 9);
        sViewsWithIds.put(R.id.guideline3, 10);
        sViewsWithIds.put(R.id.search_edit_text, 11);
        sViewsWithIds.put(R.id.search_text, 12);
        sViewsWithIds.put(R.id.guideline6, 13);
        sViewsWithIds.put(R.id.guideline, 14);
        sViewsWithIds.put(R.id.code_edit_text, 15);
        sViewsWithIds.put(R.id.name_edit_text, 16);
        sViewsWithIds.put(R.id.jobs_edit_text, 17);
        sViewsWithIds.put(R.id.guideline5, 18);
        sViewsWithIds.put(R.id.textView7, 19);
        sViewsWithIds.put(R.id.guideline4, 20);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback13;
    @Nullable
    private final android.view.View.OnClickListener mCallback11;
    @Nullable
    private final android.view.View.OnClickListener mCallback14;
    @Nullable
    private final android.view.View.OnClickListener mCallback12;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentCareerBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private FragmentCareerBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[6]
            , (com.google.android.material.textfield.TextInputLayout) bindings[15]
            , (com.google.android.material.textfield.TextInputEditText) bindings[2]
            , (android.widget.Button) bindings[7]
            , (android.widget.EditText) bindings[5]
            , (androidx.constraintlayout.widget.Guideline) bindings[14]
            , (androidx.constraintlayout.widget.Guideline) bindings[10]
            , (androidx.constraintlayout.widget.Guideline) bindings[20]
            , (androidx.constraintlayout.widget.Guideline) bindings[18]
            , (androidx.constraintlayout.widget.Guideline) bindings[13]
            , (com.google.android.material.textfield.TextInputLayout) bindings[17]
            , (com.google.android.material.textfield.TextInputEditText) bindings[4]
            , (com.google.android.material.textfield.TextInputLayout) bindings[16]
            , (com.google.android.material.textfield.TextInputEditText) bindings[3]
            , (android.widget.Button) bindings[1]
            , (com.google.android.material.textfield.TextInputLayout) bindings[11]
            , (com.google.android.material.textfield.TextInputEditText) bindings[12]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[19]
            , (android.widget.Button) bindings[8]
            );
        this.addButton.setTag(null);
        this.codeText.setTag(null);
        this.deleteButton.setTag(null);
        this.descrText.setTag(null);
        this.jobsText.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.nameText.setTag(null);
        this.searchButton.setTag(null);
        this.updateButton.setTag(null);
        setRootTag(root);
        // listeners
        mCallback13 = new com.example.uniguide2.generated.callback.OnClickListener(this, 3);
        mCallback11 = new com.example.uniguide2.generated.callback.OnClickListener(this, 1);
        mCallback14 = new com.example.uniguide2.generated.callback.OnClickListener(this, 4);
        mCallback12 = new com.example.uniguide2.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.group == variableId) {
            setGroup((com.example.uniguide2.data.Career) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((com.example.uniguide2.CareerFragment) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setGroup(@Nullable com.example.uniguide2.data.Career Group) {
        this.mGroup = Group;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.group);
        super.requestRebind();
    }
    public void setCallback(@Nullable com.example.uniguide2.CareerFragment Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String groupOccupations = null;
        com.example.uniguide2.data.Career group = mGroup;
        com.example.uniguide2.CareerFragment callback = mCallback;
        java.lang.String groupGroupName = null;
        java.lang.String groupDescription = null;
        java.lang.String groupGroupCode = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (group != null) {
                    // read group.occupations
                    groupOccupations = group.getOccupations();
                    // read group.groupName
                    groupGroupName = group.getGroupName();
                    // read group.description
                    groupDescription = group.getDescription();
                    // read group.groupCode
                    groupGroupCode = group.getGroupCode();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.addButton.setOnClickListener(mCallback12);
            this.deleteButton.setOnClickListener(mCallback13);
            this.searchButton.setOnClickListener(mCallback11);
            this.updateButton.setOnClickListener(mCallback14);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.codeText, groupGroupCode);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.descrText, groupDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.jobsText, groupOccupations);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.nameText, groupGroupName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 3: {
                // localize variables for thread safety
                // callback
                com.example.uniguide2.CareerFragment callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.deleteGroup();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // callback
                com.example.uniguide2.CareerFragment callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.searchGroup();
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // callback
                com.example.uniguide2.CareerFragment callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.updateGroup();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // callback
                com.example.uniguide2.CareerFragment callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.addGroup();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): group
        flag 1 (0x2L): callback
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}