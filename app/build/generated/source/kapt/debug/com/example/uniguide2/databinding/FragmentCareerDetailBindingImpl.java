package com.example.uniguide2.databinding;
import com.example.uniguide2.R;
import com.example.uniguide2.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentCareerDetailBindingImpl extends FragmentCareerDetailBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.divider, 4);
        sViewsWithIds.put(R.id.textView10, 5);
        sViewsWithIds.put(R.id.textView17, 6);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentCareerDetailBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private FragmentCareerDetailBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.view.View) bindings[4]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            );
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.textView4.setTag(null);
        this.textView6.setTag(null);
        this.textView8.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.group == variableId) {
            setGroup((com.example.uniguide2.data.Career) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setGroup(@Nullable com.example.uniguide2.data.Career Group) {
        this.mGroup = Group;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.group);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String groupOccupations = null;
        java.lang.String groupDescription = null;
        com.example.uniguide2.data.Career group = mGroup;
        java.lang.String groupGroupName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (group != null) {
                    // read group.occupations
                    groupOccupations = group.getOccupations();
                    // read group.description
                    groupDescription = group.getDescription();
                    // read group.groupName
                    groupGroupName = group.getGroupName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView4, groupGroupName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView6, groupDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView8, groupOccupations);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): group
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}