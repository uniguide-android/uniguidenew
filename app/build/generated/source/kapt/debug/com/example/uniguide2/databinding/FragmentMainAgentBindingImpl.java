package com.example.uniguide2.databinding;
import com.example.uniguide2.R;
import com.example.uniguide2.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMainAgentBindingImpl extends FragmentMainAgentBinding implements com.example.uniguide2.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.guideline2, 7);
        sViewsWithIds.put(R.id.cardview_image, 8);
        sViewsWithIds.put(R.id.divider, 9);
        sViewsWithIds.put(R.id.textView13, 10);
        sViewsWithIds.put(R.id.textView14, 11);
        sViewsWithIds.put(R.id.textView15, 12);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback19;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMainAgentBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 13, sIncludes, sViewsWithIds));
    }
    private FragmentMainAgentBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[8]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[3]
            , (android.view.View) bindings[9]
            , (com.google.android.material.floatingactionbutton.FloatingActionButton) bindings[6]
            , (androidx.constraintlayout.widget.Guideline) bindings[7]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[4]
            );
        this.contentTextview.setTag(null);
        this.descriptionTv.setTag(null);
        this.fab.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.uniNewsContent.setTag(null);
        this.uniNewsTitle.setTag(null);
        this.usernameTextview.setTag(null);
        setRootTag(root);
        // listeners
        mCallback19 = new com.example.uniguide2.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.profile == variableId) {
            setProfile((com.example.uniguide2.data.Agent) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((com.example.uniguide2.MainAgentFragment) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setProfile(@Nullable com.example.uniguide2.data.Agent Profile) {
        this.mProfile = Profile;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.profile);
        super.requestRebind();
    }
    public void setCallback(@Nullable com.example.uniguide2.MainAgentFragment Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.example.uniguide2.data.Agent profile = mProfile;
        java.lang.String profileEmail = null;
        java.lang.String profilePhoneNo = null;
        java.lang.String profileAddress = null;
        java.lang.String profileDescription = null;
        com.example.uniguide2.MainAgentFragment callback = mCallback;
        java.lang.String profileName = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (profile != null) {
                    // read profile.email
                    profileEmail = profile.getEmail();
                    // read profile.phoneNo
                    profilePhoneNo = profile.getPhoneNo();
                    // read profile.address
                    profileAddress = profile.getAddress();
                    // read profile.description
                    profileDescription = profile.getDescription();
                    // read profile.name
                    profileName = profile.getName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.contentTextview, profileEmail);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.descriptionTv, profileDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.uniNewsContent, profileAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.uniNewsTitle, profilePhoneNo);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.usernameTextview, profileName);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.fab.setOnClickListener(mCallback19);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // callback
        com.example.uniguide2.MainAgentFragment callback = mCallback;
        // callback != null
        boolean callbackJavaLangObjectNull = false;



        callbackJavaLangObjectNull = (callback) != (null);
        if (callbackJavaLangObjectNull) {


            callback.addNews();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): profile
        flag 1 (0x2L): callback
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}