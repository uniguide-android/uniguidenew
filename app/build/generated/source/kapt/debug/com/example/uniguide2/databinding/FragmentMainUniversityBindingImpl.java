package com.example.uniguide2.databinding;
import com.example.uniguide2.R;
import com.example.uniguide2.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMainUniversityBindingImpl extends FragmentMainUniversityBinding implements com.example.uniguide2.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.cardview_image, 6);
        sViewsWithIds.put(R.id.divider, 7);
        sViewsWithIds.put(R.id.divider4, 8);
        sViewsWithIds.put(R.id.guideline7, 9);
        sViewsWithIds.put(R.id.textView11, 10);
        sViewsWithIds.put(R.id.textView12, 11);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback7;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMainUniversityBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds));
    }
    private FragmentMainUniversityBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[6]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[2]
            , (android.view.View) bindings[7]
            , (android.view.View) bindings[8]
            , (com.google.android.material.floatingactionbutton.FloatingActionButton) bindings[5]
            , (androidx.constraintlayout.widget.Guideline) bindings[9]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[3]
            );
        this.contentTextview.setTag(null);
        this.descriptionTv.setTag(null);
        this.fab.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.uniNewsTitle.setTag(null);
        this.usernameTextview.setTag(null);
        setRootTag(root);
        // listeners
        mCallback7 = new com.example.uniguide2.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.uni == variableId) {
            setUni((com.example.uniguide2.data.University) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((com.example.uniguide2.MainUniversityFragment) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setUni(@Nullable com.example.uniguide2.data.University Uni) {
        this.mUni = Uni;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.uni);
        super.requestRebind();
    }
    public void setCallback(@Nullable com.example.uniguide2.MainUniversityFragment Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String uniPhoneNo = null;
        com.example.uniguide2.data.University uni = mUni;
        java.lang.String uniWebsite = null;
        java.lang.String uniDescription = null;
        com.example.uniguide2.MainUniversityFragment callback = mCallback;
        java.lang.String uniName = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (uni != null) {
                    // read uni.phoneNo
                    uniPhoneNo = uni.getPhoneNo();
                    // read uni.website
                    uniWebsite = uni.getWebsite();
                    // read uni.description
                    uniDescription = uni.getDescription();
                    // read uni.name
                    uniName = uni.getName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.contentTextview, uniWebsite);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.descriptionTv, uniDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.uniNewsTitle, uniPhoneNo);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.usernameTextview, uniName);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.fab.setOnClickListener(mCallback7);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // callback
        com.example.uniguide2.MainUniversityFragment callback = mCallback;
        // callback != null
        boolean callbackJavaLangObjectNull = false;



        callbackJavaLangObjectNull = (callback) != (null);
        if (callbackJavaLangObjectNull) {


            callback.uniPost();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): uni
        flag 1 (0x2L): callback
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}