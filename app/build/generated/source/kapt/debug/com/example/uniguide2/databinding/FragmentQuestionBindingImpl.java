package com.example.uniguide2.databinding;
import com.example.uniguide2.R;
import com.example.uniguide2.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentQuestionBindingImpl extends FragmentQuestionBinding implements com.example.uniguide2.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.textView3, 7);
        sViewsWithIds.put(R.id.guideline3, 8);
        sViewsWithIds.put(R.id.search_edit_text, 9);
        sViewsWithIds.put(R.id.search_text, 10);
        sViewsWithIds.put(R.id.guideline6, 11);
        sViewsWithIds.put(R.id.guideline, 12);
        sViewsWithIds.put(R.id.code_edit_text, 13);
        sViewsWithIds.put(R.id.guideline5, 14);
        sViewsWithIds.put(R.id.textView7, 15);
        sViewsWithIds.put(R.id.guideline4, 16);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback4;
    @Nullable
    private final android.view.View.OnClickListener mCallback2;
    @Nullable
    private final android.view.View.OnClickListener mCallback3;
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentQuestionBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 17, sIncludes, sViewsWithIds));
    }
    private FragmentQuestionBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[4]
            , (com.google.android.material.textfield.TextInputLayout) bindings[13]
            , (com.google.android.material.textfield.TextInputEditText) bindings[2]
            , (android.widget.Button) bindings[5]
            , (android.widget.EditText) bindings[3]
            , (androidx.constraintlayout.widget.Guideline) bindings[12]
            , (androidx.constraintlayout.widget.Guideline) bindings[8]
            , (androidx.constraintlayout.widget.Guideline) bindings[16]
            , (androidx.constraintlayout.widget.Guideline) bindings[14]
            , (androidx.constraintlayout.widget.Guideline) bindings[11]
            , (android.widget.Button) bindings[1]
            , (com.google.android.material.textfield.TextInputLayout) bindings[9]
            , (com.google.android.material.textfield.TextInputEditText) bindings[10]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[15]
            , (android.widget.Button) bindings[6]
            );
        this.addButton.setTag(null);
        this.codeText.setTag(null);
        this.deleteButton.setTag(null);
        this.descrText.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.searchButton.setTag(null);
        this.updateButton.setTag(null);
        setRootTag(root);
        // listeners
        mCallback4 = new com.example.uniguide2.generated.callback.OnClickListener(this, 4);
        mCallback2 = new com.example.uniguide2.generated.callback.OnClickListener(this, 2);
        mCallback3 = new com.example.uniguide2.generated.callback.OnClickListener(this, 3);
        mCallback1 = new com.example.uniguide2.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.callback == variableId) {
            setCallback((com.example.uniguide2.QuestionFragment) variable);
        }
        else if (BR.question == variableId) {
            setQuestion((com.example.uniguide2.data.CareerQuestion) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setCallback(@Nullable com.example.uniguide2.QuestionFragment Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }
    public void setQuestion(@Nullable com.example.uniguide2.data.CareerQuestion Question) {
        this.mQuestion = Question;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.question);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String questionCareerCode = null;
        com.example.uniguide2.QuestionFragment callback = mCallback;
        com.example.uniguide2.data.CareerQuestion question = mQuestion;
        java.lang.String questionCareerQuestion = null;

        if ((dirtyFlags & 0x6L) != 0) {



                if (question != null) {
                    // read question.careerCode
                    questionCareerCode = question.getCareerCode();
                    // read question.careerQuestion
                    questionCareerQuestion = question.getCareerQuestion();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.addButton.setOnClickListener(mCallback2);
            this.deleteButton.setOnClickListener(mCallback3);
            this.searchButton.setOnClickListener(mCallback1);
            this.updateButton.setOnClickListener(mCallback4);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.codeText, questionCareerCode);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.descrText, questionCareerQuestion);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 4: {
                // localize variables for thread safety
                // callback
                com.example.uniguide2.QuestionFragment callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.updateQuestion();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // callback
                com.example.uniguide2.QuestionFragment callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.addQuestion();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // callback
                com.example.uniguide2.QuestionFragment callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.deleteQuestion();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // callback
                com.example.uniguide2.QuestionFragment callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.searchQuestion();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): callback
        flag 1 (0x2L): question
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}