package com.example.uniguide2.databinding;
import com.example.uniguide2.R;
import com.example.uniguide2.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentSignupBindingImpl extends FragmentSignupBinding implements com.example.uniguide2.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.name_text_input, 9);
        sViewsWithIds.put(R.id.username_text_input, 10);
        sViewsWithIds.put(R.id.password_text_input, 11);
        sViewsWithIds.put(R.id.email_text_input, 12);
        sViewsWithIds.put(R.id.textView3, 13);
        sViewsWithIds.put(R.id.textView4, 14);
        sViewsWithIds.put(R.id.textView5, 15);
        sViewsWithIds.put(R.id.textView6, 16);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback22;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentSignupBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 17, sIncludes, sViewsWithIds));
    }
    private FragmentSignupBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.EditText) bindings[7]
            , (com.google.android.material.textfield.TextInputEditText) bindings[4]
            , (com.google.android.material.textfield.TextInputLayout) bindings[12]
            , (android.widget.EditText) bindings[6]
            , (com.google.android.material.textfield.TextInputEditText) bindings[1]
            , (com.google.android.material.textfield.TextInputLayout) bindings[9]
            , (com.google.android.material.textfield.TextInputLayout) bindings[11]
            , (android.widget.EditText) bindings[5]
            , (com.google.android.material.button.MaterialButton) bindings[8]
            , (com.google.android.material.textfield.TextInputEditText) bindings[3]
            , (com.google.android.material.textfield.TextInputEditText) bindings[2]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[16]
            , (com.google.android.material.textfield.TextInputLayout) bindings[10]
            );
        this.aboutEditText.setTag(null);
        this.emailEditText.setTag(null);
        this.gradeEditText.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.nameEditText.setTag(null);
        this.schoolEditText.setTag(null);
        this.signupButton.setTag(null);
        this.signupPasswordEditText.setTag(null);
        this.signupUsernameEditText.setTag(null);
        setRootTag(root);
        // listeners
        mCallback22 = new com.example.uniguide2.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.callback == variableId) {
            setCallback((com.example.uniguide2.SignUpFragment) variable);
        }
        else if (BR.stud == variableId) {
            setStud((com.example.uniguide2.data.Student) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setCallback(@Nullable com.example.uniguide2.SignUpFragment Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }
    public void setStud(@Nullable com.example.uniguide2.data.Student Stud) {
        this.mStud = Stud;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.stud);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String studUserUsername = null;
        java.lang.String studUserPassword = null;
        java.lang.String studAbout = null;
        com.example.uniguide2.SignUpFragment callback = mCallback;
        java.lang.String studName = null;
        java.lang.String studSchool = null;
        com.example.uniguide2.data.Student stud = mStud;
        java.lang.String studGrade = null;
        com.example.uniguide2.data.User studUser = null;
        java.lang.String studEmail = null;

        if ((dirtyFlags & 0x6L) != 0) {



                if (stud != null) {
                    // read stud.about
                    studAbout = stud.getAbout();
                    // read stud.name
                    studName = stud.getName();
                    // read stud.school
                    studSchool = stud.getSchool();
                    // read stud.grade
                    studGrade = stud.getGrade();
                    // read stud.user
                    studUser = stud.getUser();
                    // read stud.email
                    studEmail = stud.getEmail();
                }


                if (studUser != null) {
                    // read stud.user.username
                    studUserUsername = studUser.getUsername();
                    // read stud.user.password
                    studUserPassword = studUser.getPassword();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.aboutEditText, studAbout);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.emailEditText, studEmail);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.gradeEditText, studGrade);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.nameEditText, studName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.schoolEditText, studSchool);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.signupPasswordEditText, studUserPassword);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.signupUsernameEditText, studUserUsername);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.signupButton.setOnClickListener(mCallback22);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // callback
        com.example.uniguide2.SignUpFragment callback = mCallback;
        // callback != null
        boolean callbackJavaLangObjectNull = false;



        callbackJavaLangObjectNull = (callback) != (null);
        if (callbackJavaLangObjectNull) {


            callback.signStudent();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): callback
        flag 1 (0x2L): stud
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}