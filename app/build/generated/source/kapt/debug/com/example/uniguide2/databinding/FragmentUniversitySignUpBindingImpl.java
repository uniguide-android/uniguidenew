package com.example.uniguide2.databinding;
import com.example.uniguide2.R;
import com.example.uniguide2.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentUniversitySignUpBindingImpl extends FragmentUniversitySignUpBinding implements com.example.uniguide2.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.signUp_tv, 8);
        sViewsWithIds.put(R.id.textInputLayout4, 9);
        sViewsWithIds.put(R.id.textInputLayout3, 10);
        sViewsWithIds.put(R.id.guideline3, 11);
        sViewsWithIds.put(R.id.guideline4, 12);
        sViewsWithIds.put(R.id.guideline5, 13);
        sViewsWithIds.put(R.id.textInputLayout5, 14);
        sViewsWithIds.put(R.id.textInputLayout2, 15);
        sViewsWithIds.put(R.id.confirmPassword_et, 16);
        sViewsWithIds.put(R.id.textInputLayout9, 17);
        sViewsWithIds.put(R.id.textInputLayout10, 18);
        sViewsWithIds.put(R.id.textInputLayout11, 19);
    }
    // views
    @NonNull
    private final android.widget.ScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback20;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentUniversitySignUpBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 20, sIncludes, sViewsWithIds));
    }
    private FragmentUniversitySignUpBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.textfield.TextInputEditText) bindings[1]
            , (com.google.android.material.textfield.TextInputEditText) bindings[5]
            , (com.google.android.material.textfield.TextInputEditText) bindings[16]
            , (com.google.android.material.textfield.TextInputEditText) bindings[6]
            , (androidx.constraintlayout.widget.Guideline) bindings[11]
            , (androidx.constraintlayout.widget.Guideline) bindings[12]
            , (androidx.constraintlayout.widget.Guideline) bindings[13]
            , (com.google.android.material.button.MaterialButton) bindings[7]
            , (com.google.android.material.textfield.TextInputEditText) bindings[3]
            , (com.google.android.material.textfield.TextInputEditText) bindings[4]
            , (android.widget.TextView) bindings[8]
            , (com.google.android.material.textfield.TextInputLayout) bindings[18]
            , (com.google.android.material.textfield.TextInputLayout) bindings[19]
            , (com.google.android.material.textfield.TextInputLayout) bindings[15]
            , (com.google.android.material.textfield.TextInputLayout) bindings[10]
            , (com.google.android.material.textfield.TextInputLayout) bindings[9]
            , (com.google.android.material.textfield.TextInputLayout) bindings[14]
            , (com.google.android.material.textfield.TextInputLayout) bindings[17]
            , (com.google.android.material.textfield.TextInputEditText) bindings[2]
            );
        this.NameEt.setTag(null);
        this.addressEt.setTag(null);
        this.descriptionEt.setTag(null);
        this.loginButton.setTag(null);
        this.mboundView0 = (android.widget.ScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.passwordEt.setTag(null);
        this.phoneNumberEt.setTag(null);
        this.userNameEt.setTag(null);
        setRootTag(root);
        // listeners
        mCallback20 = new com.example.uniguide2.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.uni == variableId) {
            setUni((com.example.uniguide2.data.University) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((com.example.uniguide2.UniversitySignUpFragment) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setUni(@Nullable com.example.uniguide2.data.University Uni) {
        this.mUni = Uni;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.uni);
        super.requestRebind();
    }
    public void setCallback(@Nullable com.example.uniguide2.UniversitySignUpFragment Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String uniUserUsername = null;
        java.lang.String uniUserPassword = null;
        com.example.uniguide2.data.University uni = mUni;
        java.lang.String uniWebsite = null;
        java.lang.String uniDescription = null;
        com.example.uniguide2.UniversitySignUpFragment callback = mCallback;
        java.lang.String uniPhoneNo = null;
        com.example.uniguide2.data.User uniUser = null;
        java.lang.String uniName = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (uni != null) {
                    // read uni.website
                    uniWebsite = uni.getWebsite();
                    // read uni.description
                    uniDescription = uni.getDescription();
                    // read uni.phoneNo
                    uniPhoneNo = uni.getPhoneNo();
                    // read uni.user
                    uniUser = uni.getUser();
                    // read uni.name
                    uniName = uni.getName();
                }


                if (uniUser != null) {
                    // read uni.user.username
                    uniUserUsername = uniUser.getUsername();
                    // read uni.user.password
                    uniUserPassword = uniUser.getPassword();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.NameEt, uniName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.addressEt, uniWebsite);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.descriptionEt, uniDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.passwordEt, uniUserPassword);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.phoneNumberEt, uniPhoneNo);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.userNameEt, uniUserUsername);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.loginButton.setOnClickListener(mCallback20);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // callback
        com.example.uniguide2.UniversitySignUpFragment callback = mCallback;
        // callback != null
        boolean callbackJavaLangObjectNull = false;



        callbackJavaLangObjectNull = (callback) != (null);
        if (callbackJavaLangObjectNull) {


            callback.signUniversity();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): uni
        flag 1 (0x2L): callback
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}