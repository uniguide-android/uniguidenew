package com.example.uniguide2.databinding;
import com.example.uniguide2.R;
import com.example.uniguide2.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LayoutCareerItemBindingImpl extends LayoutCareerItemBinding implements com.example.uniguide2.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final com.google.android.material.card.MaterialCardView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback21;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LayoutCareerItemBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 2, sIncludes, sViewsWithIds));
    }
    private LayoutCareerItemBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[1]
            );
        this.careerName.setTag(null);
        this.mboundView0 = (com.google.android.material.card.MaterialCardView) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        mCallback21 = new com.example.uniguide2.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.group == variableId) {
            setGroup((com.example.uniguide2.data.Career) variable);
        }
        else if (BR.itemClickListener == variableId) {
            setItemClickListener((com.example.uniguide2.eventHandler.CareerEventListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setGroup(@Nullable com.example.uniguide2.data.Career Group) {
        this.mGroup = Group;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.group);
        super.requestRebind();
    }
    public void setItemClickListener(@Nullable com.example.uniguide2.eventHandler.CareerEventListener ItemClickListener) {
        this.mItemClickListener = ItemClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.itemClickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.example.uniguide2.data.Career group = mGroup;
        com.example.uniguide2.eventHandler.CareerEventListener itemClickListener = mItemClickListener;
        java.lang.String groupGroupName = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (group != null) {
                    // read group.groupName
                    groupGroupName = group.getGroupName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.careerName, groupGroupName);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.careerName.setOnClickListener(mCallback21);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // group
        com.example.uniguide2.data.Career group = mGroup;
        // itemClickListener
        com.example.uniguide2.eventHandler.CareerEventListener itemClickListener = mItemClickListener;
        // itemClickListener != null
        boolean itemClickListenerJavaLangObjectNull = false;



        itemClickListenerJavaLangObjectNull = (itemClickListener) != (null);
        if (itemClickListenerJavaLangObjectNull) {



            itemClickListener.viewDetails(group);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): group
        flag 1 (0x2L): itemClickListener
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}