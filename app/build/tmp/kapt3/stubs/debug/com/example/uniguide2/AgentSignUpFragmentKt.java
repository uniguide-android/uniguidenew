package com.example.uniguide2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000&\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u0001X\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0001X\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"address", "", "agentBinding", "Lcom/example/uniguide2/databinding/FragmentAgentSignUpBinding;", "agentViewModel", "Lcom/example/uniguide2/viewmodel/AgentViewModel;", "confirmPassword", "description", "email", "listener", "Lcom/example/uniguide2/AgentSignUpFragment$OnFragmentInteractionListener;", "name", "options", "Landroidx/navigation/NavOptions;", "param1", "param2", "password", "phoneNumber", "userName", "app_debug"})
public final class AgentSignUpFragmentKt {
    private static com.example.uniguide2.databinding.FragmentAgentSignUpBinding agentBinding;
    private static com.example.uniguide2.viewmodel.AgentViewModel agentViewModel;
    private static androidx.navigation.NavOptions options;
    private static java.lang.String param1;
    private static java.lang.String param2;
    private static java.lang.String name;
    private static java.lang.String userName;
    private static java.lang.String password;
    private static java.lang.String confirmPassword;
    private static java.lang.String email;
    private static java.lang.String phoneNumber;
    private static java.lang.String address;
    private static java.lang.String description;
    private static com.example.uniguide2.AgentSignUpFragment.OnFragmentInteractionListener listener;
}