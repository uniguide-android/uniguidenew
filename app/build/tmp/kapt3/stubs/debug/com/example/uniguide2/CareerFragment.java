package com.example.uniguide2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0006\u0010\b\u001a\u00020\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u0006\u001a\u00020\u0007H\u0002J\u0010\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0006\u001a\u00020\u0007H\u0002J&\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u0006\u0010\u0014\u001a\u00020\u0004J\u0016\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0016\u001a\u00020\nJ\u0006\u0010\u0017\u001a\u00020\u0004\u00a8\u0006\u0018"}, d2 = {"Lcom/example/uniguide2/CareerFragment;", "Landroidx/fragment/app/Fragment;", "()V", "addGroup", "", "clearFields", "binding", "Lcom/example/uniguide2/databinding/FragmentCareerBinding;", "deleteGroup", "getGroup", "Lcom/example/uniguide2/data/Career;", "getGroupToAdd", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "searchGroup", "setFields", "group", "updateGroup", "app_debug"})
public final class CareerFragment extends androidx.fragment.app.Fragment {
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final com.example.uniguide2.data.Career getGroup(com.example.uniguide2.databinding.FragmentCareerBinding binding) {
        return null;
    }
    
    private final com.example.uniguide2.data.Career getGroupToAdd(com.example.uniguide2.databinding.FragmentCareerBinding binding) {
        return null;
    }
    
    public final void clearFields(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.databinding.FragmentCareerBinding binding) {
    }
    
    public final void setFields(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.databinding.FragmentCareerBinding binding, @org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group) {
    }
    
    public final void addGroup() {
    }
    
    public final void updateGroup() {
    }
    
    public final void deleteGroup() {
    }
    
    public final void searchGroup() {
    }
    
    public CareerFragment() {
        super();
    }
}