package com.example.uniguide2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"binding", "Lcom/example/uniguide2/databinding/FragmentCareerBinding;", "myContainer", "Landroid/view/ViewGroup;", "viewModel", "Lcom/example/uniguide2/viewmodel/CareerGroupViewModel;", "app_debug"})
public final class CareerFragmentKt {
    private static android.view.ViewGroup myContainer;
    private static com.example.uniguide2.viewmodel.CareerGroupViewModel viewModel;
    private static com.example.uniguide2.databinding.FragmentCareerBinding binding;
}