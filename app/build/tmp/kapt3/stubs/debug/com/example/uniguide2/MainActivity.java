package com.example.uniguide2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u00062\u00020\u00072\u00020\b2\u00020\t2\u00020\n2\u00020\u000b2\u00020\f2\u00020\rB\u0005\u00a2\u0006\u0002\u0010\u000eJ\u0012\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0014J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\b\u0010\u001c\u001a\u00020\u0016H\u0016J\u0018\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0010H\u0002J\u0010\u0010!\u001a\u00020\u00122\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0010\u0010\"\u001a\u00020\u00122\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0010\u0010#\u001a\u00020\u00122\u0006\u0010$\u001a\u00020%H\u0016R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"}, d2 = {"Lcom/example/uniguide2/MainActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/example/uniguide2/eventHandler/ViewDetailHandler;", "Lcom/example/uniguide2/LoginFragment$OnFragmentInteractionListener;", "Lcom/example/uniguide2/StudentsMainFragment$OnFragmentInteractionListener;", "Lcom/example/uniguide2/PostFragment$OnFragmentInteractionListener;", "Lcom/example/uniguide2/SignUpFragment$OnFragmentInteractionListener;", "Lcom/example/uniguide2/AgentSignUpFragment$OnFragmentInteractionListener;", "Lcom/example/uniguide2/MainAgentFragment$OnFragmentInteractionListener;", "Lcom/example/uniguide2/AgentPostFragment$OnFragmentInteractionListener;", "Lcom/example/uniguide2/UniPostFragment$OnFragmentInteractionListener;", "Lcom/example/uniguide2/MainUniversityFragment$OnFragmentInteractionListener;", "Lcom/example/uniguide2/UniversitySignUpFragment$OnFragmentInteractionListener;", "Lcom/example/uniguide2/NewsFragment$OnFragmentInteractionListener;", "()V", "appBarConfiguration", "Landroidx/navigation/ui/AppBarConfiguration;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onSupportNavigateUp", "setupActionBar", "navController", "Landroidx/navigation/NavController;", "appBarConfig", "setupBottomNavMenu", "setupNavigationMenu", "viewCareerDetails", "group", "Lcom/example/uniguide2/data/Career;", "app_debug"})
public final class MainActivity extends androidx.appcompat.app.AppCompatActivity implements com.example.uniguide2.eventHandler.ViewDetailHandler, com.example.uniguide2.LoginFragment.OnFragmentInteractionListener, com.example.uniguide2.StudentsMainFragment.OnFragmentInteractionListener, com.example.uniguide2.PostFragment.OnFragmentInteractionListener, com.example.uniguide2.SignUpFragment.OnFragmentInteractionListener, com.example.uniguide2.AgentSignUpFragment.OnFragmentInteractionListener, com.example.uniguide2.MainAgentFragment.OnFragmentInteractionListener, com.example.uniguide2.AgentPostFragment.OnFragmentInteractionListener, com.example.uniguide2.UniPostFragment.OnFragmentInteractionListener, com.example.uniguide2.MainUniversityFragment.OnFragmentInteractionListener, com.example.uniguide2.UniversitySignUpFragment.OnFragmentInteractionListener, com.example.uniguide2.NewsFragment.OnFragmentInteractionListener {
    private androidx.navigation.ui.AppBarConfiguration appBarConfiguration;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void viewCareerDetails(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setupBottomNavMenu(androidx.navigation.NavController navController) {
    }
    
    private final void setupNavigationMenu(androidx.navigation.NavController navController) {
    }
    
    private final void setupActionBar(androidx.navigation.NavController navController, androidx.navigation.ui.AppBarConfiguration appBarConfig) {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onSupportNavigateUp() {
        return false;
    }
    
    public MainActivity() {
        super();
    }
}