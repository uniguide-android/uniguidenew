package com.example.uniguide2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000\"\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000\"\u001a\u0010\b\u001a\u00020\tX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u000e"}, d2 = {"agentViewModel", "Lcom/example/uniguide2/viewmodel/AgentViewModel;", "listener", "Lcom/example/uniguide2/MainAgentFragment$OnFragmentInteractionListener;", "mainBinding", "Lcom/example/uniguide2/databinding/FragmentMainAgentBinding;", "options", "Landroidx/navigation/NavOptions;", "sharedPref", "Landroid/content/SharedPreferences;", "getSharedPref", "()Landroid/content/SharedPreferences;", "setSharedPref", "(Landroid/content/SharedPreferences;)V", "app_debug"})
public final class MainAgentFragmentKt {
    private static com.example.uniguide2.databinding.FragmentMainAgentBinding mainBinding;
    private static com.example.uniguide2.viewmodel.AgentViewModel agentViewModel;
    private static androidx.navigation.NavOptions options;
    @org.jetbrains.annotations.NotNull()
    public static android.content.SharedPreferences sharedPref;
    private static com.example.uniguide2.MainAgentFragment.OnFragmentInteractionListener listener;
    
    @org.jetbrains.annotations.NotNull()
    public static final android.content.SharedPreferences getSharedPref() {
        return null;
    }
    
    public static final void setSharedPref(@org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences p0) {
    }
}