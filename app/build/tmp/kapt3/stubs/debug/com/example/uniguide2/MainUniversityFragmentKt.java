package com.example.uniguide2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\"\u0010\u0010\u0000\u001a\u0004\u0018\u00010\u0001X\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082.\u00a2\u0006\u0002\n\u0000\"\u001a\u0010\u0004\u001a\u00020\u0005X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\"\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"listener", "Lcom/example/uniguide2/MainUniversityFragment$OnFragmentInteractionListener;", "options", "Landroidx/navigation/NavOptions;", "sharedPrefer", "Landroid/content/SharedPreferences;", "getSharedPrefer", "()Landroid/content/SharedPreferences;", "setSharedPrefer", "(Landroid/content/SharedPreferences;)V", "uniBinding", "Lcom/example/uniguide2/databinding/FragmentMainUniversityBinding;", "uniViewModel", "Lcom/example/uniguide2/viewmodel/UniversityViewModel;", "app_debug"})
public final class MainUniversityFragmentKt {
    private static com.example.uniguide2.databinding.FragmentMainUniversityBinding uniBinding;
    private static com.example.uniguide2.viewmodel.UniversityViewModel uniViewModel;
    private static androidx.navigation.NavOptions options;
    @org.jetbrains.annotations.NotNull()
    public static android.content.SharedPreferences sharedPrefer;
    private static com.example.uniguide2.MainUniversityFragment.OnFragmentInteractionListener listener;
    
    @org.jetbrains.annotations.NotNull()
    public static final android.content.SharedPreferences getSharedPrefer() {
        return null;
    }
    
    public static final void setSharedPrefer(@org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences p0) {
    }
}