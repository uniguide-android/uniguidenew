package com.example.uniguide2;

import java.lang.System;

/**
 * * A simple [Fragment] subclass.
 * * Activities that contain this fragment must implement the
 * * [NewsFragment.OnFragmentInteractionListener] interface
 * * to handle interaction events.
 * * Use the [NewsFragment.newInstance] factory method to
 * * create an instance of this fragment.
 * *
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00162\u00020\u0001:\u0002\u0016\u0017B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0012\u0010\f\u001a\u00020\t2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J&\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u00142\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\b\u0010\u0015\u001a\u00020\tH\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"}, d2 = {"Lcom/example/uniguide2/NewsFragment;", "Landroidx/fragment/app/Fragment;", "()V", "listener", "Lcom/example/uniguide2/NewsFragment$OnFragmentInteractionListener;", "param1", "", "param2", "onAttach", "", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDetach", "Companion", "OnFragmentInteractionListener", "app_debug"})
public final class NewsFragment extends androidx.fragment.app.Fragment {
    private java.lang.String param1;
    private java.lang.String param2;
    private com.example.uniguide2.NewsFragment.OnFragmentInteractionListener listener;
    public static final com.example.uniguide2.NewsFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @java.lang.Override()
    public void onDetach() {
    }
    
    public NewsFragment() {
        super();
    }
    
    /**
     * * Use this factory method to create a new instance of
     *         * this fragment using the provided parameters.
     *         *
     *         * @param param1 Parameter 1.
     *         * @param param2 Parameter 2.
     *         * @return A new instance of fragment NewsFragment.
     */
    @org.jetbrains.annotations.NotNull()
    public static final com.example.uniguide2.NewsFragment newInstance(@org.jetbrains.annotations.NotNull()
    java.lang.String param1, @org.jetbrains.annotations.NotNull()
    java.lang.String param2) {
        return null;
    }
    
    /**
     * * This interface must be implemented by activities that contain this
     *     * fragment to allow an interaction in this fragment to be communicated
     *     * to the activity and potentially other fragments contained in that
     *     * activity.
     *     *
     *     *
     *     * See the Android Training lesson [Communicating with Other Fragments]
     *     * (http://developer.android.com/training/basics/fragments/communicating.html)
     *     * for more information.
     */
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\bf\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"}, d2 = {"Lcom/example/uniguide2/NewsFragment$OnFragmentInteractionListener;", "", "app_debug"})
    public static abstract interface OnFragmentInteractionListener {
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0007\u00a8\u0006\b"}, d2 = {"Lcom/example/uniguide2/NewsFragment$Companion;", "", "()V", "newInstance", "Lcom/example/uniguide2/NewsFragment;", "param1", "", "param2", "app_debug"})
    public static final class Companion {
        
        /**
         * * Use this factory method to create a new instance of
         *         * this fragment using the provided parameters.
         *         *
         *         * @param param1 Parameter 1.
         *         * @param param2 Parameter 2.
         *         * @return A new instance of fragment NewsFragment.
         */
        @org.jetbrains.annotations.NotNull()
        public final com.example.uniguide2.NewsFragment newInstance(@org.jetbrains.annotations.NotNull()
        java.lang.String param1, @org.jetbrains.annotations.NotNull()
        java.lang.String param2) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}