package com.example.uniguide2;

import java.lang.System;

/**
 * * A simple [Fragment] subclass.
 * * Activities that contain this fragment must implement the
 * * [PostFragment.OnFragmentInteractionListener] interface
 * * to handle interaction events.
 * * Use the [PostFragment.newInstance] factory method to
 * * create an instance of this fragment.
 * *
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 A2\u00020\u0001:\u0002ABB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010,\u001a\u00020-J\b\u0010.\u001a\u00020/H\u0002J\u0010\u00100\u001a\u00020-2\u0006\u00101\u001a\u000202H\u0016J\u0012\u00103\u001a\u00020-2\b\u00104\u001a\u0004\u0018\u000105H\u0016J&\u00106\u001a\u0004\u0018\u0001072\u0006\u00108\u001a\u0002092\b\u0010:\u001a\u0004\u0018\u00010;2\b\u00104\u001a\u0004\u0018\u000105H\u0016J\b\u0010<\u001a\u00020-H\u0016J\u0010\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020@H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u00020\rX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u00020\u0013X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001a\u001a\u00020\u001bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u000e\u0010 \u001a\u00020!X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010&\u001a\u00020\'X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+\u00a8\u0006C"}, d2 = {"Lcom/example/uniguide2/PostFragment;", "Landroidx/fragment/app/Fragment;", "()V", "listener", "Lcom/example/uniguide2/PostFragment$OnFragmentInteractionListener;", "options", "Landroidx/navigation/NavOptions;", "param1", "", "param2", "postBinding", "Lcom/example/uniguide2/databinding/FragmentPostBinding;", "postButton", "Landroid/widget/Button;", "getPostButton", "()Landroid/widget/Button;", "setPostButton", "(Landroid/widget/Button;)V", "postEditText", "Landroid/widget/EditText;", "getPostEditText", "()Landroid/widget/EditText;", "setPostEditText", "(Landroid/widget/EditText;)V", "postViewModel", "Lcom/example/uniguide2/viewmodel/PostViewModel;", "sharedPref", "Landroid/content/SharedPreferences;", "getSharedPref", "()Landroid/content/SharedPreferences;", "setSharedPref", "(Landroid/content/SharedPreferences;)V", "studentViewModel", "Lcom/example/uniguide2/viewmodel/StudentViewModel;", "user", "Lcom/example/uniguide2/data/User;", "userViewModel", "Lcom/example/uniguide2/viewmodel/UserViewModel;", "usernameTextView", "Landroid/widget/TextView;", "getUsernameTextView", "()Landroid/widget/TextView;", "setUsernameTextView", "(Landroid/widget/TextView;)V", "addPost", "", "connected", "", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDetach", "readFields", "Lcom/example/uniguide2/data/Post;", "student", "Lcom/example/uniguide2/data/Student;", "Companion", "OnFragmentInteractionListener", "app_debug"})
public final class PostFragment extends androidx.fragment.app.Fragment {
    private java.lang.String param1;
    private java.lang.String param2;
    private com.example.uniguide2.PostFragment.OnFragmentInteractionListener listener;
    private com.example.uniguide2.databinding.FragmentPostBinding postBinding;
    private com.example.uniguide2.viewmodel.UserViewModel userViewModel;
    private com.example.uniguide2.viewmodel.PostViewModel postViewModel;
    private com.example.uniguide2.viewmodel.StudentViewModel studentViewModel;
    private com.example.uniguide2.data.User user;
    private androidx.navigation.NavOptions options;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView usernameTextView;
    @org.jetbrains.annotations.NotNull()
    public android.widget.Button postButton;
    @org.jetbrains.annotations.NotNull()
    public android.widget.EditText postEditText;
    @org.jetbrains.annotations.NotNull()
    public android.content.SharedPreferences sharedPref;
    public static final com.example.uniguide2.PostFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getUsernameTextView() {
        return null;
    }
    
    public final void setUsernameTextView(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.Button getPostButton() {
        return null;
    }
    
    public final void setPostButton(@org.jetbrains.annotations.NotNull()
    android.widget.Button p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.EditText getPostEditText() {
        return null;
    }
    
    public final void setPostEditText(@org.jetbrains.annotations.NotNull()
    android.widget.EditText p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.SharedPreferences getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences p0) {
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @java.lang.Override()
    public void onDetach() {
    }
    
    private final com.example.uniguide2.data.Post readFields(com.example.uniguide2.data.Student student) {
        return null;
    }
    
    private final boolean connected() {
        return false;
    }
    
    public final void addPost() {
    }
    
    public PostFragment() {
        super();
    }
    
    /**
     * * Use this factory method to create a new instance of
     *         * this fragment using the provided parameters.
     *         *
     *         * @param param1 Parameter 1.
     *         * @param param2 Parameter 2.
     *         * @return A new instance of fragment PostFragment.
     */
    @org.jetbrains.annotations.NotNull()
    public static final com.example.uniguide2.PostFragment newInstance(@org.jetbrains.annotations.NotNull()
    java.lang.String param1, @org.jetbrains.annotations.NotNull()
    java.lang.String param2) {
        return null;
    }
    
    /**
     * * This interface must be implemented by activities that contain this
     *     * fragment to allow an interaction in this fragment to be communicated
     *     * to the activity and potentially other fragments contained in that
     *     * activity.
     *     *
     *     *
     *     * See the Android Training lesson [Communicating with Other Fragments]
     *     * (http://developer.android.com/training/basics/fragments/communicating.html)
     *     * for more information.
     */
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\bf\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"}, d2 = {"Lcom/example/uniguide2/PostFragment$OnFragmentInteractionListener;", "", "app_debug"})
    public static abstract interface OnFragmentInteractionListener {
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0007\u00a8\u0006\b"}, d2 = {"Lcom/example/uniguide2/PostFragment$Companion;", "", "()V", "newInstance", "Lcom/example/uniguide2/PostFragment;", "param1", "", "param2", "app_debug"})
    public static final class Companion {
        
        /**
         * * Use this factory method to create a new instance of
         *         * this fragment using the provided parameters.
         *         *
         *         * @param param1 Parameter 1.
         *         * @param param2 Parameter 2.
         *         * @return A new instance of fragment PostFragment.
         */
        @org.jetbrains.annotations.NotNull()
        public final com.example.uniguide2.PostFragment newInstance(@org.jetbrains.annotations.NotNull()
        java.lang.String param1, @org.jetbrains.annotations.NotNull()
        java.lang.String param2) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}