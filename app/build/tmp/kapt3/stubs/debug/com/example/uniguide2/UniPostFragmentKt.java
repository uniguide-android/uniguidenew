package com.example.uniguide2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u00002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\"\u0010\u0010\u0000\u001a\u0004\u0018\u00010\u0001X\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"listener", "Lcom/example/uniguide2/UniPostFragment$OnFragmentInteractionListener;", "newsViewModel", "Lcom/example/uniguide2/viewmodel/NewsViewModel;", "options", "Landroidx/navigation/NavOptions;", "postBinding", "Lcom/example/uniguide2/databinding/FragmentUniPostBinding;", "sharedPrefPost", "Landroid/content/SharedPreferences;", "uniViewModel", "Lcom/example/uniguide2/viewmodel/UniversityViewModel;", "user", "Lcom/example/uniguide2/data/User;", "userViewModel", "Lcom/example/uniguide2/viewmodel/UserViewModel;", "app_debug"})
public final class UniPostFragmentKt {
    private static com.example.uniguide2.viewmodel.UserViewModel userViewModel;
    private static com.example.uniguide2.viewmodel.NewsViewModel newsViewModel;
    private static com.example.uniguide2.viewmodel.UniversityViewModel uniViewModel;
    private static androidx.navigation.NavOptions options;
    private static com.example.uniguide2.data.User user;
    private static com.example.uniguide2.databinding.FragmentUniPostBinding postBinding;
    private static android.content.SharedPreferences sharedPrefPost;
    private static com.example.uniguide2.UniPostFragment.OnFragmentInteractionListener listener;
}