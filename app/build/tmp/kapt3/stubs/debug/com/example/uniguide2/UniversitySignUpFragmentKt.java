package com.example.uniguide2;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\"\u0010\u0010\u0000\u001a\u0004\u0018\u00010\u0001X\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"listener", "Lcom/example/uniguide2/UniversitySignUpFragment$OnFragmentInteractionListener;", "options", "Landroidx/navigation/NavOptions;", "uniSignBinding", "Lcom/example/uniguide2/databinding/FragmentUniversitySignUpBinding;", "uniViewModel", "Lcom/example/uniguide2/viewmodel/UniversityViewModel;", "app_debug"})
public final class UniversitySignUpFragmentKt {
    private static com.example.uniguide2.databinding.FragmentUniversitySignUpBinding uniSignBinding;
    private static com.example.uniguide2.viewmodel.UniversityViewModel uniViewModel;
    private static androidx.navigation.NavOptions options;
    private static com.example.uniguide2.UniversitySignUpFragment.OnFragmentInteractionListener listener;
}