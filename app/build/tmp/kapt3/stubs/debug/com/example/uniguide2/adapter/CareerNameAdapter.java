package com.example.uniguide2.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u00012\u00020\u0003:\u0001\u0017B\u001b\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u00a2\u0006\u0002\u0010\tJ\b\u0010\u000b\u001a\u00020\fH\u0016J\u001c\u0010\r\u001a\u00020\u000e2\n\u0010\u000f\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0010\u001a\u00020\fH\u0016J\u001c\u0010\u0011\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\fH\u0016J\u0010\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\bH\u0016R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"}, d2 = {"Lcom/example/uniguide2/adapter/CareerNameAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/example/uniguide2/adapter/CareerNameAdapter$CareerGroupHolder;", "Lcom/example/uniguide2/eventHandler/CareerEventListener;", "context", "Landroid/content/Context;", "data", "", "Lcom/example/uniguide2/data/Career;", "(Landroid/content/Context;Ljava/util/List;)V", "careerList", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "viewDetails", "group", "CareerGroupHolder", "app_debug"})
public final class CareerNameAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.example.uniguide2.adapter.CareerNameAdapter.CareerGroupHolder> implements com.example.uniguide2.eventHandler.CareerEventListener {
    private final android.content.Context context = null;
    private java.util.List<com.example.uniguide2.data.Career> careerList;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.uniguide2.adapter.CareerNameAdapter.CareerGroupHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.adapter.CareerNameAdapter.CareerGroupHolder holder, int position) {
    }
    
    @java.lang.Override()
    public void viewDetails(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group) {
    }
    
    public CareerNameAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.uniguide2.data.Career> data) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/example/uniguide2/adapter/CareerNameAdapter$CareerGroupHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lcom/example/uniguide2/databinding/LayoutCareerItemBinding;", "(Lcom/example/uniguide2/adapter/CareerNameAdapter;Lcom/example/uniguide2/databinding/LayoutCareerItemBinding;)V", "getBinding", "()Lcom/example/uniguide2/databinding/LayoutCareerItemBinding;", "app_debug"})
    public final class CareerGroupHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final com.example.uniguide2.databinding.LayoutCareerItemBinding binding = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.example.uniguide2.databinding.LayoutCareerItemBinding getBinding() {
            return null;
        }
        
        public CareerGroupHolder(@org.jetbrains.annotations.NotNull()
        com.example.uniguide2.databinding.LayoutCareerItemBinding binding) {
            super(null);
        }
    }
}