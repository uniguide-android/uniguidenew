package com.example.uniguide2.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0001\u001dB\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0012H\u0016J\u0018\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0012H\u0016J\u001b\u0010\u001b\u001a\u00020\u00142\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fH\u0000\u00a2\u0006\u0002\b\u001cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0019\u0010\t\u001a\n \u000b*\u0004\u0018\u00010\n0\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lcom/example/uniguide2/adapter/PostAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/example/uniguide2/adapter/PostAdapter$PostViewHolder;", "Lcom/example/uniguide2/eventHandler/PostEventHandler;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getContext", "()Landroid/content/Context;", "inflater", "Landroid/view/LayoutInflater;", "kotlin.jvm.PlatformType", "getInflater", "()Landroid/view/LayoutInflater;", "posts", "", "Lcom/example/uniguide2/data/Post;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setPosts", "setPosts$app_debug", "PostViewHolder", "app_debug"})
public final class PostAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.example.uniguide2.adapter.PostAdapter.PostViewHolder> implements com.example.uniguide2.eventHandler.PostEventHandler {
    private final android.view.LayoutInflater inflater = null;
    private java.util.List<com.example.uniguide2.data.Post> posts;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    
    public final android.view.LayoutInflater getInflater() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.uniguide2.adapter.PostAdapter.PostViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.adapter.PostAdapter.PostViewHolder holder, int position) {
    }
    
    public final void setPosts$app_debug(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.uniguide2.data.Post> posts) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public PostAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/example/uniguide2/adapter/PostAdapter$PostViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lcom/example/uniguide2/databinding/RecyclerViewItemBinding;", "(Lcom/example/uniguide2/databinding/RecyclerViewItemBinding;)V", "getBinding", "()Lcom/example/uniguide2/databinding/RecyclerViewItemBinding;", "app_debug"})
    public static final class PostViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final com.example.uniguide2.databinding.RecyclerViewItemBinding binding = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.example.uniguide2.databinding.RecyclerViewItemBinding getBinding() {
            return null;
        }
        
        public PostViewHolder(@org.jetbrains.annotations.NotNull()
        com.example.uniguide2.databinding.RecyclerViewItemBinding binding) {
            super(null);
        }
    }
}