package com.example.uniguide2.data;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\'J\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00032\u0006\u0010\u0007\u001a\u00020\bH\'J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0005H\'\u00a8\u0006\f"}, d2 = {"Lcom/example/uniguide2/data/GroupDao;", "", "getAllGroups", "Landroidx/lifecycle/LiveData;", "", "Lcom/example/uniguide2/data/Career;", "getCareerGroupByCode", "code", "", "insertGroup", "", "group", "app_debug"})
public abstract interface GroupDao {
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract long insertGroup(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "Select * from career_groups where group_code= :code")
    public abstract androidx.lifecycle.LiveData<com.example.uniguide2.data.Career> getCareerGroupByCode(@org.jetbrains.annotations.NotNull()
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "Select * from career_groups")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.example.uniguide2.data.Career>> getAllGroups();
}