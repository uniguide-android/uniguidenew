package com.example.uniguide2.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\bf\u0018\u0000 \u000e2\u00020\u0001:\u0001\u000eJ\u001e\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u0007H\'J\u001e\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u0007H\'J\u001e\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u0007H\'J\u001e\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00040\u00032\b\b\u0001\u0010\f\u001a\u00020\tH\'J(\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\f\u001a\u00020\tH\'\u00a8\u0006\u000f"}, d2 = {"Lcom/example/uniguide2/network/AgentApiService;", "", "deleteAgentAsync", "Lkotlinx/coroutines/Deferred;", "Lretrofit2/Response;", "Ljava/lang/Void;", "id", "", "findByIdAsync", "Lcom/example/uniguide2/data/Agent;", "findByUserIdAsync", "registerAgentAsync", "newAgent", "updateAgentAsync", "Companion", "app_debug"})
public abstract interface AgentApiService {
    public static final com.example.uniguide2.network.AgentApiService.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "agent/{id}")
    public abstract kotlinx.coroutines.Deferred<retrofit2.Response<com.example.uniguide2.data.Agent>> findByIdAsync(@retrofit2.http.Path(value = "id")
    long id);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "agent/user/{id}")
    public abstract kotlinx.coroutines.Deferred<retrofit2.Response<com.example.uniguide2.data.Agent>> findByUserIdAsync(@retrofit2.http.Path(value = "id")
    long id);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "agent/register")
    public abstract kotlinx.coroutines.Deferred<retrofit2.Response<com.example.uniguide2.data.Agent>> registerAgentAsync(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.example.uniguide2.data.Agent newAgent);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.PUT(value = "agent/{id}")
    public abstract kotlinx.coroutines.Deferred<retrofit2.Response<com.example.uniguide2.data.Agent>> updateAgentAsync(@retrofit2.http.Path(value = "id")
    long id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.example.uniguide2.data.Agent newAgent);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.DELETE(value = "agent/{id}")
    public abstract kotlinx.coroutines.Deferred<retrofit2.Response<java.lang.Void>> deleteAgentAsync(@retrofit2.http.Path(value = "id")
    long id);
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0006R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/example/uniguide2/network/AgentApiService$Companion;", "", "()V", "baseUrl", "", "getInstance", "Lcom/example/uniguide2/network/AgentApiService;", "app_debug"})
    public static final class Companion {
        private static final java.lang.String baseUrl = "http://192.168.137.1:8090/";
        
        @org.jetbrains.annotations.NotNull()
        public final com.example.uniguide2.network.AgentApiService getInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}