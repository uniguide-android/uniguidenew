package com.example.uniguide2.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\tH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ\u001f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u00062\u0006\u0010\b\u001a\u00020\tH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ\u001f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u00062\u0006\u0010\b\u001a\u00020\tH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\f0\u00062\u0006\u0010\u000f\u001a\u00020\fH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010J\'\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\f0\u00062\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\fH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0012R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0013"}, d2 = {"Lcom/example/uniguide2/repository/AgentRepository;", "", "agentApiService", "Lcom/example/uniguide2/network/AgentApiService;", "(Lcom/example/uniguide2/network/AgentApiService;)V", "deleteAgent", "Lretrofit2/Response;", "Ljava/lang/Void;", "id", "", "(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;", "findById", "Lcom/example/uniguide2/data/Agent;", "findByUserId", "registerAgent", "newAgent", "(Lcom/example/uniguide2/data/Agent;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateAgent", "(JLcom/example/uniguide2/data/Agent;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class AgentRepository {
    private final com.example.uniguide2.network.AgentApiService agentApiService = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object findById(long id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.example.uniguide2.data.Agent>> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object findByUserId(long id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.example.uniguide2.data.Agent>> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object registerAgent(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Agent newAgent, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.example.uniguide2.data.Agent>> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object updateAgent(long id, @org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Agent newAgent, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.example.uniguide2.data.Agent>> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object deleteAgent(long id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<java.lang.Void>> p1) {
        return null;
    }
    
    public AgentRepository(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.network.AgentApiService agentApiService) {
        super();
    }
}