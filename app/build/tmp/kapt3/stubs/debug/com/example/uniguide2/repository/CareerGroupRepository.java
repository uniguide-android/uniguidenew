package com.example.uniguide2.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0011\u0010\u0007\u001a\u00020\bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u0012\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0\u000bJ\u000e\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\rJ\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\r0\u000b2\u0006\u0010\u0011\u001a\u00020\u0012J\u0019\u0010\u0013\u001a\u00020\b2\u0006\u0010\u0011\u001a\u00020\u0012H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0014J\u0019\u0010\u0015\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\rH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016J\u000e\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u000f\u001a\u00020\rJ\u000e\u0010\u0019\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\rR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u001a"}, d2 = {"Lcom/example/uniguide2/repository/CareerGroupRepository;", "", "groupDao", "Lcom/example/uniguide2/data/GroupDao;", "careerGroupApiService", "Lcom/example/uniguide2/network/CareerApiService;", "(Lcom/example/uniguide2/data/GroupDao;Lcom/example/uniguide2/network/CareerApiService;)V", "allGroupsApi", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "allGroupsRoom", "Landroidx/lifecycle/LiveData;", "", "Lcom/example/uniguide2/data/Career;", "deleteGroupApi", "group", "getAGroup", "code", "", "getGroupByCodeApi", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "insertGroupApi", "(Lcom/example/uniguide2/data/Career;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "insertGroupRoom", "", "updateGroupApi", "app_debug"})
public final class CareerGroupRepository {
    private final com.example.uniguide2.data.GroupDao groupDao = null;
    private final com.example.uniguide2.network.CareerApiService careerGroupApiService = null;
    
    public final long insertGroupRoom(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group) {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.example.uniguide2.data.Career>> allGroupsRoom() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.example.uniguide2.data.Career> getAGroup(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object insertGroupApi(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    public final void deleteGroupApi(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group) {
    }
    
    public final void updateGroupApi(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getGroupByCodeApi(@org.jetbrains.annotations.NotNull()
    java.lang.String code, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object allGroupsApi(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p0) {
        return null;
    }
    
    public CareerGroupRepository(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.GroupDao groupDao, @org.jetbrains.annotations.NotNull()
    com.example.uniguide2.network.CareerApiService careerGroupApiService) {
        super();
    }
}