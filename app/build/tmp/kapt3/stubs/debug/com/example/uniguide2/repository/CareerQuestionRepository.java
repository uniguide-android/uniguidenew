package com.example.uniguide2.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bJ\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\nJ\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\n0\b2\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0011\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\nJ\u000e\u0010\u0012\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0013"}, d2 = {"Lcom/example/uniguide2/repository/CareerQuestionRepository;", "", "questionDao", "Lcom/example/uniguide2/data/QuestionDao;", "(Lcom/example/uniguide2/data/QuestionDao;)V", "getQuestionDao", "()Lcom/example/uniguide2/data/QuestionDao;", "allQuestions", "Landroidx/lifecycle/LiveData;", "", "Lcom/example/uniguide2/data/CareerQuestion;", "deleteQuestion", "", "question", "getQuestionByDescr", "code", "", "insertQuestion", "updateQuestion", "app_debug"})
public final class CareerQuestionRepository {
    @org.jetbrains.annotations.NotNull()
    private final com.example.uniguide2.data.QuestionDao questionDao = null;
    
    public final void insertQuestion(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.CareerQuestion question) {
    }
    
    public final void deleteQuestion(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.CareerQuestion question) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.example.uniguide2.data.CareerQuestion>> allQuestions() {
        return null;
    }
    
    public final void updateQuestion(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.CareerQuestion question) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.example.uniguide2.data.CareerQuestion> getQuestionByDescr(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.uniguide2.data.QuestionDao getQuestionDao() {
        return null;
    }
    
    public CareerQuestionRepository(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.QuestionDao questionDao) {
        super();
    }
}