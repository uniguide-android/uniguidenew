package com.example.uniguide2.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H&J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0005H&J\u0010\u0010\t\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u000bH&J\u0016\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u00032\u0006\u0010\n\u001a\u00020\u000bH&J\u0010\u0010\r\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0005H&J\u0010\u0010\u000e\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0005H&\u00a8\u0006\u000f"}, d2 = {"Lcom/example/uniguide2/repository/GroupRepository;", "", "allGroups", "Landroidx/lifecycle/LiveData;", "", "Lcom/example/uniguide2/data/Career;", "deleteGroup", "", "group", "getAGroup", "code", "", "getGroupByCode", "insertGroup", "updateGroup", "app_debug"})
public abstract interface GroupRepository {
    
    public abstract void insertGroup(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group);
    
    public abstract void deleteGroup(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group);
    
    public abstract void updateGroup(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group);
    
    @org.jetbrains.annotations.NotNull()
    public abstract androidx.lifecycle.LiveData<com.example.uniguide2.data.Career> getGroupByCode(@org.jetbrains.annotations.NotNull()
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    public abstract androidx.lifecycle.LiveData<java.util.List<com.example.uniguide2.data.Career>> allGroups();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.example.uniguide2.data.Career getAGroup(@org.jetbrains.annotations.NotNull()
    java.lang.String code);
}