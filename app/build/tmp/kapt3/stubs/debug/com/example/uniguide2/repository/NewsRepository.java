package com.example.uniguide2.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010J\u0011\u0010\u0011\u001a\u00020\u0012H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010J\u001f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000f0\b2\u0006\u0010\n\u001a\u00020\u000bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ\u001f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u000f0\b2\u0006\u0010\u0015\u001a\u00020\u000fH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016J\u000e\u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\u000fJ\'\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000f0\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\u000fH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0019R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u001a"}, d2 = {"Lcom/example/uniguide2/repository/NewsRepository;", "", "newsDao", "Lcom/example/uniguide2/data/NewsDao;", "newsApiService", "Lcom/example/uniguide2/network/NewsApiService;", "(Lcom/example/uniguide2/data/NewsDao;Lcom/example/uniguide2/network/NewsApiService;)V", "deleteNewsApi", "Lretrofit2/Response;", "Ljava/lang/Void;", "id", "", "(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;", "findAll", "", "Lcom/example/uniguide2/data/News;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "findAllApi", "", "findByIdApi", "saveNewsApi", "newNews", "(Lcom/example/uniguide2/data/News;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "saveNewsRoom", "updateNewsApi", "(JLcom/example/uniguide2/data/News;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class NewsRepository {
    private final com.example.uniguide2.data.NewsDao newsDao = null;
    private final com.example.uniguide2.network.NewsApiService newsApiService = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object saveNewsApi(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.News newNews, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.example.uniguide2.data.News>> p1) {
        return null;
    }
    
    public final long saveNewsRoom(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.News newNews) {
        return 0L;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object findAllApi(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p0) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object findByIdApi(long id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.example.uniguide2.data.News>> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object updateNewsApi(long id, @org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.News newNews, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.example.uniguide2.data.News>> p2) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object deleteNewsApi(long id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<java.lang.Void>> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object findAll(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.example.uniguide2.data.News>> p0) {
        return null;
    }
    
    public NewsRepository(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.NewsDao newsDao, @org.jetbrains.annotations.NotNull()
    com.example.uniguide2.network.NewsApiService newsApiService) {
        super();
    }
}