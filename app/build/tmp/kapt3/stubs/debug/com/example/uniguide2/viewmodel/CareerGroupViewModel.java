package com.example.uniguide2.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\bJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u000e\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\bJ\u0014\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\b0\u00062\u0006\u0010\u001a\u001a\u00020\rJ\u000e\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\bR\u001d\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\b0\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"}, d2 = {"Lcom/example/uniguide2/viewmodel/CareerGroupViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "allGroups", "Landroidx/lifecycle/LiveData;", "", "Lcom/example/uniguide2/data/Career;", "getAllGroups", "()Landroidx/lifecycle/LiveData;", "filterData", "Landroidx/lifecycle/MutableLiveData;", "", "groupRepository", "Lcom/example/uniguide2/repository/CareerGroupRepository;", "searchData", "addCareerGroup", "Lkotlinx/coroutines/Job;", "group", "connected", "", "context", "Landroid/content/Context;", "deleteCareerGroup", "getGroupByCode", "code", "updateCareerGroup", "app_debug"})
public final class CareerGroupViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.example.uniguide2.repository.CareerGroupRepository groupRepository = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.List<com.example.uniguide2.data.Career>> allGroups = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.String> filterData = null;
    private androidx.lifecycle.LiveData<com.example.uniguide2.data.Career> searchData;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.example.uniguide2.data.Career>> getAllGroups() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job addCareerGroup(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job deleteCareerGroup(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job updateCareerGroup(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.Career group) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.example.uniguide2.data.Career> getGroupByCode(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
        return null;
    }
    
    private final boolean connected(android.content.Context context) {
        return false;
    }
    
    public CareerGroupViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}