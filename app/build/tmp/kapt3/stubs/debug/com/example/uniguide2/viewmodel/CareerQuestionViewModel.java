package com.example.uniguide2.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\bJ\u000e\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\bJ\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\b0\u00062\u0006\u0010\u0014\u001a\u00020\u000bJ\u000e\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\bR\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\b0\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lcom/example/uniguide2/viewmodel/CareerQuestionViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "allQuestions", "Landroidx/lifecycle/LiveData;", "", "Lcom/example/uniguide2/data/CareerQuestion;", "filterData", "Landroidx/lifecycle/MutableLiveData;", "", "questionRepository", "Lcom/example/uniguide2/repository/CareerQuestionRepository;", "searchData", "addCareerQuestion", "Lkotlinx/coroutines/Job;", "question", "deleteCareerQuestion", "getGroupByQuestion", "code", "updateCareerQuestion", "app_debug"})
public final class CareerQuestionViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.example.uniguide2.repository.CareerQuestionRepository questionRepository = null;
    private final androidx.lifecycle.LiveData<java.util.List<com.example.uniguide2.data.CareerQuestion>> allQuestions = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.String> filterData = null;
    private androidx.lifecycle.LiveData<com.example.uniguide2.data.CareerQuestion> searchData;
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job addCareerQuestion(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.CareerQuestion question) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job deleteCareerQuestion(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.CareerQuestion question) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job updateCareerQuestion(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.CareerQuestion question) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.example.uniguide2.data.CareerQuestion> getGroupByQuestion(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
        return null;
    }
    
    public CareerQuestionViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}