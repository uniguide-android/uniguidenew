package com.example.uniguide2.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0002J\u000e\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%J\u0006\u0010&\u001a\u00020#J\u000e\u0010\'\u001a\u00020#2\u0006\u0010$\u001a\u00020%J\u000e\u0010(\u001a\u00020#2\u0006\u0010)\u001a\u00020\nJ\u0016\u0010*\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010)\u001a\u00020\nR\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\f0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00068F\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u001d\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u00070\u00138F\u00a2\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015R\u001d\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\f0\u00138F\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0015R\u001d\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u00070\u00138F\u00a2\u0006\u0006\u001a\u0004\b\u0019\u0010\u0015R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u001c\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u00070\u00138F\u00a2\u0006\u0006\u001a\u0004\b\u001d\u0010\u0015\u00a8\u0006+"}, d2 = {"Lcom/example/uniguide2/viewmodel/NewsViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "_deleteResponse", "Landroidx/lifecycle/MutableLiveData;", "Lretrofit2/Response;", "Ljava/lang/Void;", "_getResponse", "Lcom/example/uniguide2/data/News;", "_getResponses", "", "_insertResponse", "_updateResponse", "deleteResponse", "getDeleteResponse", "()Landroidx/lifecycle/MutableLiveData;", "getResponse", "Landroidx/lifecycle/LiveData;", "getGetResponse", "()Landroidx/lifecycle/LiveData;", "getResponses", "getGetResponses", "insertResponse", "getInsertResponse", "newsRepository", "Lcom/example/uniguide2/repository/NewsRepository;", "updateResponse", "getUpdateResponse", "connected", "", "context", "Landroid/content/Context;", "deleteNews", "Lkotlinx/coroutines/Job;", "id", "", "findAll", "findById", "saveNews", "newNews", "updateNews", "app_debug"})
public final class NewsViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.example.uniguide2.repository.NewsRepository newsRepository = null;
    private final androidx.lifecycle.MutableLiveData<retrofit2.Response<com.example.uniguide2.data.News>> _getResponse = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.example.uniguide2.data.News>> _getResponses = null;
    private final androidx.lifecycle.MutableLiveData<retrofit2.Response<com.example.uniguide2.data.News>> _updateResponse = null;
    private final androidx.lifecycle.MutableLiveData<retrofit2.Response<com.example.uniguide2.data.News>> _insertResponse = null;
    private final androidx.lifecycle.MutableLiveData<retrofit2.Response<java.lang.Void>> _deleteResponse = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<retrofit2.Response<com.example.uniguide2.data.News>> getGetResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.example.uniguide2.data.News>> getGetResponses() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<retrofit2.Response<com.example.uniguide2.data.News>> getUpdateResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<retrofit2.Response<com.example.uniguide2.data.News>> getInsertResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<retrofit2.Response<java.lang.Void>> getDeleteResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job findById(long id) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job saveNews(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.News newNews) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job updateNews(long id, @org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.News newNews) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job findAll() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job deleteNews(long id) {
        return null;
    }
    
    private final boolean connected(android.content.Context context) {
        return false;
    }
    
    public NewsViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}