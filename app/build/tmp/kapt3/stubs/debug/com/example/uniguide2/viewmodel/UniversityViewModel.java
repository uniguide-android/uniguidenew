package com.example.uniguide2.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eJ\u000e\u0010\u001f\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eJ\u000e\u0010 \u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eJ\u000e\u0010!\u001a\u00020\u001c2\u0006\u0010\"\u001a\u00020\bJ\u0016\u0010#\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\"\u001a\u00020\bR\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00048F\u00a2\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u001d\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00050\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u001d\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00050\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u0014\u0010\u0012R\u001d\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00050\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u0016\u0010\u0012R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00050\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u0012\u00a8\u0006$"}, d2 = {"Lcom/example/uniguide2/viewmodel/UniversityViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_deleteResponse", "Landroidx/lifecycle/MutableLiveData;", "Lretrofit2/Response;", "Ljava/lang/Void;", "_getByUserResponse", "Lcom/example/uniguide2/data/University;", "_getResponse", "_insertResponse", "_updateResponse", "deleteResponse", "getDeleteResponse", "()Landroidx/lifecycle/MutableLiveData;", "getByUserResponse", "Landroidx/lifecycle/LiveData;", "getGetByUserResponse", "()Landroidx/lifecycle/LiveData;", "getResponse", "getGetResponse", "insertResponse", "getInsertResponse", "universityRepository", "Lcom/example/uniguide2/repository/UniversityRepository;", "updateResponse", "getUpdateResponse", "deleteUniversity", "Lkotlinx/coroutines/Job;", "id", "", "findById", "findByUserId", "registerUniversity", "newUniversity", "updateUniversity", "app_debug"})
public final class UniversityViewModel extends androidx.lifecycle.ViewModel {
    private final com.example.uniguide2.repository.UniversityRepository universityRepository = null;
    private final androidx.lifecycle.MutableLiveData<retrofit2.Response<com.example.uniguide2.data.University>> _getResponse = null;
    private final androidx.lifecycle.MutableLiveData<retrofit2.Response<com.example.uniguide2.data.University>> _getByUserResponse = null;
    private final androidx.lifecycle.MutableLiveData<retrofit2.Response<com.example.uniguide2.data.University>> _updateResponse = null;
    private final androidx.lifecycle.MutableLiveData<retrofit2.Response<com.example.uniguide2.data.University>> _insertResponse = null;
    private final androidx.lifecycle.MutableLiveData<retrofit2.Response<java.lang.Void>> _deleteResponse = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<retrofit2.Response<com.example.uniguide2.data.University>> getGetResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<retrofit2.Response<com.example.uniguide2.data.University>> getGetByUserResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<retrofit2.Response<com.example.uniguide2.data.University>> getUpdateResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<retrofit2.Response<com.example.uniguide2.data.University>> getInsertResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<retrofit2.Response<java.lang.Void>> getDeleteResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job findById(long id) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job registerUniversity(@org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.University newUniversity) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job updateUniversity(long id, @org.jetbrains.annotations.NotNull()
    com.example.uniguide2.data.University newUniversity) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job findByUserId(long id) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job deleteUniversity(long id) {
        return null;
    }
    
    public UniversityViewModel() {
        super();
    }
}