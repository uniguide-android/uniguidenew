package com.example.uniguide2.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013J\u0016\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0016R\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\t8F\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u001d\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\t8F\u00a2\u0006\u0006\u001a\u0004\b\r\u0010\u000bR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"}, d2 = {"Lcom/example/uniguide2/viewmodel/UserViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_getByIdResponse", "Landroidx/lifecycle/MutableLiveData;", "Lretrofit2/Response;", "Lcom/example/uniguide2/data/User;", "_getResponse", "getByIdResponse", "Landroidx/lifecycle/LiveData;", "getGetByIdResponse", "()Landroidx/lifecycle/LiveData;", "getResponse", "getGetResponse", "userRepository", "Lcom/example/uniguide2/repository/UserRepository;", "findById", "Lkotlinx/coroutines/Job;", "id", "", "login", "username", "", "password", "app_debug"})
public final class UserViewModel extends androidx.lifecycle.ViewModel {
    private final com.example.uniguide2.repository.UserRepository userRepository = null;
    private final androidx.lifecycle.MutableLiveData<retrofit2.Response<com.example.uniguide2.data.User>> _getResponse = null;
    private final androidx.lifecycle.MutableLiveData<retrofit2.Response<com.example.uniguide2.data.User>> _getByIdResponse = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<retrofit2.Response<com.example.uniguide2.data.User>> getGetResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<retrofit2.Response<com.example.uniguide2.data.User>> getGetByIdResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job login(@org.jetbrains.annotations.NotNull()
    java.lang.String username, @org.jetbrains.annotations.NotNull()
    java.lang.String password) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job findById(long id) {
        return null;
    }
    
    public UserViewModel() {
        super();
    }
}