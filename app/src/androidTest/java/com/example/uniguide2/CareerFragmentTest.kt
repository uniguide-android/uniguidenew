package com.example.uniguide2

import android.content.Intent
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.example.uniguide2.data.Career
import com.example.uniguide2.data.UniguideDatabase
import com.example.uniguide2.network.CareerApiService
import com.example.uniguide2.repository.CareerGroupRepository
import com.example.uniguide2.viewmodel.CareerGroupViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.LooperMode
import org.robolectric.annotation.TextLayoutMode


@RunWith(AndroidJUnit4::class)
@MediumTest
@LooperMode(LooperMode.Mode.PAUSED)
@TextLayoutMode(TextLayoutMode.Mode.REALISTIC)
@ExperimentalCoroutinesApi
class CareerFragmentTest{

    private lateinit var repo: CareerGroupRepository
    private lateinit var database: UniguideDatabase
    private lateinit var viewModel: CareerGroupViewModel

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    @Rule
    @JvmField
    val activityRule = object : ActivityTestRule<MainActivity>(MainActivity::class.java) {
        override fun getActivityIntent(): Intent {
            return Intent(InstrumentationRegistry.getInstrumentation().context, MainActivity::class.java)

        }
    }

    @Rule
    fun rule() = activityRule

    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            UniguideDatabase::class.java,
            "uniguide_db").allowMainThreadQueries().build()

        repo = CareerGroupRepository(database.groupDao(), CareerApiService.getInstance())
        viewModel = CareerGroupViewModel(ApplicationProvider.getApplicationContext())
        rule().activity.supportFragmentManager.beginTransaction()
    }

    @After
    fun cleanUp() {
        database.close()
    }

    @Test
    fun displayTask_whenRepositoryHasData() = runBlockingTest{
        // GIVEN - One group already in the repository
       repo.insertGroupRoom(
           Career("B", "Finance","Accounting Clerk, " +
        "Appraiser","Interest in financial and investment planning and management, and providing banking and insurance services.")
       )

        // WHEN - On startup
        val scenario = launchFragmentInContainer<CareerViewFragment>()

        //scenario.moveToState(Lifecycle.State.CREATED)


        // THEN - Verify group is displayed on screen
        Espresso.onView(ViewMatchers.withText("Finance")).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}