package com.example.uniguide2.endToEndTests

import android.os.Bundle
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.uniguide2.CareerFragment
import com.example.uniguide2.MainCoroutineRule
import com.example.uniguide2.R
import com.example.uniguide2.data.Career
import com.example.uniguide2.data.UniguideDatabase
import com.example.uniguide2.network.CareerApiService
import com.example.uniguide2.repository.CareerGroupRepository
import com.example.uniguide2.viewmodel.CareerGroupViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito

@RunWith(AndroidJUnit4::class)
@LargeTest
class SearchCareerGroupTest {

    private lateinit var careerGroupViewModel: CareerGroupViewModel

    private lateinit var database: UniguideDatabase

    private lateinit var groupRepository: CareerGroupRepository

    private lateinit var result : Career


    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    var group = Career("B", "Finance","Accounting Clerk, " +
            "Appraiser","Interest in financial and investment planning and management, and providing banking and insurance services.")

    @Before
    fun setupViewModel() {
        database=
            Room.databaseBuilder(
                ApplicationProvider.getApplicationContext(),
                UniguideDatabase::class.java,
                "uniguide_db").allowMainThreadQueries().build()

        // We initialise the repository with no tasks
        groupRepository = CareerGroupRepository(database.groupDao(), CareerApiService.getInstance())

        // Create class under test
        careerGroupViewModel = CareerGroupViewModel(ApplicationProvider.getApplicationContext())


    }

    @After
    fun closeDb() = database.close()


    @Test
    fun searchGroupTest() {
        // Given a user in the home screen
        val scenario = launchFragmentInContainer<CareerFragment>(Bundle(), R.style.AppTheme)
        val navController = Mockito.mock(NavController::class.java)
        scenario.onFragment {
            Navigation.setViewNavController(it.view!!, navController)
        }

        onView(withId(R.id.search_text)).perform(typeText("B"), closeSoftKeyboard())
        onView(withId(R.id.search_button)).perform(click())

        // Then verify task is displayed on screen
        onView(withText("Finance")).check(matches(isDisplayed()))
    }

}