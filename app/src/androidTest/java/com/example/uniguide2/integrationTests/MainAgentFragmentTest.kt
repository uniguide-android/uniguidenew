package com.example.uniguide2.integrationTests

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.example.uniguide2.MainActivity
import com.example.uniguide2.MainAgentFragment
import com.example.uniguide2.R
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.annotation.LooperMode
import org.robolectric.annotation.TextLayoutMode

@RunWith(AndroidJUnit4::class)
@MediumTest
@LooperMode(LooperMode.Mode.PAUSED)
@TextLayoutMode(TextLayoutMode.Mode.REALISTIC)
@ExperimentalCoroutinesApi
class MainAgentFragmentTest {

    @get:Rule
    @Rule
    @JvmField
    val activityRule = object : ActivityTestRule<MainActivity>(MainActivity::class.java) {
        override fun getActivityIntent(): Intent {
            return Intent(InstrumentationRegistry.getInstrumentation().context, MainActivity::class.java)

        }
    }

    @Rule
    fun rule() = activityRule

    @Before
    fun setup() {
        rule().activity.supportFragmentManager.beginTransaction()
    }

    @Test
    fun clickAddNewsButton_navigateToAgentPostFragment() {
        // Given a user in the home screen
        val scenario = launchFragmentInContainer<MainAgentFragment>(Bundle(), R.style.AppTheme)
        val navController = Mockito.mock(NavController::class.java)
        scenario.onFragment {
            Navigation.setViewNavController(it.view!!, navController)
        }

        // When the FAB is clicked
        Espresso.onView(ViewMatchers.withId(R.id.fab)).perform(ViewActions.click())

        // Then verify that we navigate to the add screen
        Mockito.verify(navController).navigate(R.id.agentPostFragment)
    }
}