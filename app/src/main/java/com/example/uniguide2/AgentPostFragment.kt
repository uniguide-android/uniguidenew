package com.example.uniguide2

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.data.Agent
import com.example.uniguide2.data.News
import com.example.uniguide2.data.User
import com.example.uniguide2.databinding.FragmentAgentPostBinding
import com.example.uniguide2.viewmodel.AgentViewModel
import com.example.uniguide2.viewmodel.NewsViewModel
import com.example.uniguide2.viewmodel.UserViewModel

class AgentPostFragment : Fragment() {
    private lateinit var agentBinding: FragmentAgentPostBinding
    private lateinit var options : NavOptions
    private lateinit var userViewModel : UserViewModel
    private lateinit var newsViewModel : NewsViewModel
    private lateinit var agentViewModel: AgentViewModel
    private lateinit var user : User


    private var listener: OnFragmentInteractionListener? = null

    lateinit var sharedPref: SharedPreferences

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        agentBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_agent_post,container , false)
        val myView : View = agentBinding.root
        agentBinding.callback = this

        sharedPref = (activity as Activity).getSharedPreferences(SHARED_PREFERENCE_ID, Context.MODE_PRIVATE)

        val userId = sharedPref.getLong(ID_KEY, 0)

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        newsViewModel = ViewModelProviders.of(this).get(NewsViewModel::class.java)
        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel::class.java)

        if(connected()){
            userViewModel.findById(userId)
            userViewModel.getByIdResponse.observe(this, Observer { response ->
                response.body()?.run{
                    user = this
                    agentBinding.usernameTextview.text = this.username
                }
            })

        }else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }

        options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }

        return myView
    }

    fun readFields(agent: Agent): News {
       val title = agentBinding.uniNewsTitle.text.toString()
        val post = agentBinding.uniNewsContent.text.toString()
        return News(0, title, post, agent.user.username)
    }

    private fun connected():Boolean {

        val connectivityManager = (activity as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    fun postNews(){
        if(connected()){
            agentViewModel.findByUserId(user.id)
            agentViewModel.getByUserResponse.observe(this, Observer{response ->
                val owner = this
                response.body()?.run{
                    var news = readFields(this)

                    newsViewModel.saveNews(news)
                    newsViewModel.insertResponse.observe(owner, Observer { response ->
                        response.body()?.run{
                            findNavController().navigate(R.id.mainAgentFragment, null, options)
                        }
                    })
                }

            })


        }else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }
    }

}