package com.example.uniguide2

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.AgentSignUpFragment
import com.example.uniguide2.data.Agent
import com.example.uniguide2.data.User
import com.example.uniguide2.databinding.FragmentAgentSignUpBinding
import com.example.uniguide2.viewmodel.AgentViewModel

private lateinit var agentBinding: FragmentAgentSignUpBinding
private lateinit var agentViewModel: AgentViewModel
private lateinit var options : NavOptions

private var param1: String? = null
private var param2: String? = null

private lateinit var name: String
private lateinit var userName: String
private lateinit var password: String
private lateinit var confirmPassword: String
private lateinit var email: String
private lateinit var phoneNumber: String
private lateinit var address: String
private lateinit var description: String

private var listener: AgentSignUpFragment.OnFragmentInteractionListener? = null


class AgentSignUpFragment : Fragment() {


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            agentBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_agent_sign_up,container , false)
            val myView : View = agentBinding.root
            agentBinding.callback = this

            agentViewModel = ViewModelProviders.of(this).get(AgentViewModel::class.java)



            name = agentBinding.NameEt.text.toString()
            userName = agentBinding.userNameEt.text.toString()
            password = agentBinding.passwordEt.text.toString()
            confirmPassword = agentBinding.confirmPasswordEt.text.toString()
            email = agentBinding.emailEt.text.toString()
            phoneNumber = agentBinding.phoneNumberEt.text.toString()
            address = agentBinding.addressEt.text.toString()
            description = agentBinding.descriptionEt.text.toString()



            options = navOptions {
                anim {
                    enter = R.anim.slide_in_right
                    exit = R.anim.slide_out_left
                    popEnter = R.anim.slide_in_left
                    popExit = R.anim.slide_out_right
                }
            }


            return myView
        }
    private fun readFields(): Agent? {
        var user = if(password == confirmPassword) User(0, userName, password, emptySet()) else null
        val agent = user?.let {
            Agent(0, name,
                email,
                phoneNumber,
                address,
                description, it)
        }
        return agent


    }

    private fun connected():Boolean {

        val connectivityManager = (activity as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    fun signAgent(){
        val agent = readFields()
        if(agent != null) {
            if (connected()) {
                agentViewModel.registerAgent(agent)
                agentViewModel.insertResponse.observe(this, Observer { response ->
                    response.body()?.run {
                        findNavController().navigate(R.id.loginFragment, null, options)
                    }
                })

            }
            else{
                Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

            }

        }
        else{
            Toast.makeText(activity,"Trouble signing up...check input fields", Toast.LENGTH_SHORT).show()

        }
    }
}