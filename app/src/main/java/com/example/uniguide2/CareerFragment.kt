package com.example.uniguide2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.uniguide2.data.Career
import com.example.uniguide2.databinding.FragmentCareerBinding
import com.example.uniguide2.viewmodel.CareerGroupViewModel



private lateinit var myContainer:ViewGroup
private lateinit var viewModel: CareerGroupViewModel
private lateinit var binding:FragmentCareerBinding

class CareerFragment:Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater ,R.layout.fragment_career,container , false)
        val myView : View = binding.root
        binding.callback = this
        viewModel = ViewModelProviders.of(this).get(CareerGroupViewModel::class.java)

        container?.let{
            myContainer = it;
        }
        return myView
    }

    private fun getGroup(binding: FragmentCareerBinding) : Career {
        return Career(binding.searchText.text.toString(),binding.nameText.text.toString(),binding.jobsText.text.toString(),binding.descrText.text.toString());
    }
    private fun getGroupToAdd(binding: FragmentCareerBinding): Career {
        return Career(binding.codeText.text.toString(),binding.nameText.text.toString(),binding.jobsText.text.toString(),binding.descrText.text.toString());
    }



    fun clearFields(binding: FragmentCareerBinding){
        binding.nameText.setText("")
        binding.codeText.setText("")
        binding.descrText.setText("")
        binding.jobsText.setText("")
        binding.searchText.setText("")
    }

    fun setFields(binding: FragmentCareerBinding,group: Career){
        binding.nameText.setText(group.groupName)
        binding.descrText.setText(group.description)
        binding.jobsText.setText(group.occupations)
    }


    fun addGroup() {
        binding?.let {
            viewModel.addCareerGroup(getGroupToAdd(binding))
            Toast.makeText(myContainer?.context, getGroup(binding).groupCode + "Career Group has been added successfully", Toast.LENGTH_SHORT)
                    .show()
            clearFields(binding)
        }
    }

    fun updateGroup(){
        binding?.let{
            if(!binding.searchText.text.toString().isNullOrEmpty()) {
                viewModel.updateCareerGroup(getGroup(it))
                Toast.makeText(myContainer?.context, "Career Group has been updated successfully", Toast.LENGTH_SHORT)
                    .show()
            }
            else{
                Toast.makeText(myContainer?.context, "Can't add Career Group", Toast.LENGTH_SHORT)
                    .show()
            }
            clearFields(binding)

        }
    }

    fun deleteGroup(){
        binding?.let{
            viewModel.deleteCareerGroup(getGroup(it))
            Toast.makeText(myContainer?.context,"Career Group has been deleted successfully",Toast.LENGTH_SHORT).show()
            clearFields(it)
        }

    }

    fun searchGroup(){
        val codeSearch = binding.searchText.text.toString()
        viewModel.getGroupByCode(codeSearch).observe(this, Observer{
                group->group?.let {
            setFields(binding,group)
        }
        })

    }


}


