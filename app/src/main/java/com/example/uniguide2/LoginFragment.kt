package com.example.uniguide2

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.data.Role
import com.example.uniguide2.data.Student
import com.example.uniguide2.data.User
import com.example.uniguide2.databinding.FragmentCareerBinding
import com.example.uniguide2.databinding.FragmentLoginBinding
import com.example.uniguide2.network.StudentApiService
import com.example.uniguide2.network.UserApiService
import com.example.uniguide2.viewmodel.AgentViewModel
import com.example.uniguide2.viewmodel.StudentViewModel
import com.example.uniguide2.viewmodel.UniversityViewModel
import com.example.uniguide2.viewmodel.UserViewModel
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.fragment_agent_sign_up.view.*
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [LoginFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */




class LoginFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null



    private lateinit var loginBinding:FragmentLoginBinding
    private lateinit var userViewModel: UserViewModel
    private lateinit var options : NavOptions


    lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        loginBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_login,container , false)
        val myView : View = loginBinding.root
        loginBinding.callback = this


        sharedPref = (listener as Activity).getSharedPreferences(SHARED_PREFERENCE_ID, Context.MODE_PRIVATE)

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        options = navOptions {
                        anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }


        return myView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }
    override fun onDetach() {
        super.onDetach()
        listener = null
    }
    interface OnFragmentInteractionListener {


    }

        companion object {
            /**
             * Use this factory method to create a new instance of
             * this fragment using the provided parameters.
             *
             * @param param1 Parameter 1.
             * @param param2 Parameter 2.
             * @return A new instance of fragment SignupFragment.
             */
            // TODO: Rename and change types and number of parameters
            @JvmStatic
            fun newInstance(param1: String, param2: String) =
                LoginFragment().apply {
                    arguments = Bundle().apply {
                        putString(com.example.uniguide2.ARG_PARAM1, param1)
                        putString(com.example.uniguide2.ARG_PARAM2, param2)
                    }
                }
        }

    private fun connected():Boolean {

        val connectivityManager = (listener as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    fun loginUser() {
        loginBinding?.let {
            if (connected()) {
                val username = it.usernameEditText.text.toString()
                val password = it.passwordEditText.text.toString()
                userViewModel.login(username,password)
                userViewModel.getResponse.observe(this, Observer { response ->
                    response.body()?.run {
                        var user = this
                        var args = Bundle()
                        args.putSerializable("currentUser", user)
                        with(sharedPref.edit()) {
                            clear()
                            commit()
                        }
                        with(sharedPref.edit()) {
                            putLong(ID_KEY, user.id)
                            commit()
                        }

                        user.roles.forEach {
                            if (it.role == "STUDENT") {
                                findNavController().navigate(R.id.universitiesMainFragment, args, options)
                            } else if (it.role == "AGENT") {
                                findNavController().navigate(R.id.mainAgentFragment, args, options)
                            } else {
                                findNavController().navigate(R.id.mainUnifragment, args, options)
                            }
                        }

                    }
                })


            }
            else{
                Toast.makeText(activity,"Please connect to the Internet to proceed",Toast.LENGTH_SHORT).show()
            }
        }




    }
    fun goToAgentSignUp(){
        findNavController().navigate(R.id.agentSignUpfragment, null, options)
    }
    fun goToStudentSignUp(){
        findNavController().navigate(R.id.signupFragment, null, options)
    }
    fun goToUniSignUp(){
        findNavController().navigate(R.id.universitySignUpfragment, null, options)
    }
}
