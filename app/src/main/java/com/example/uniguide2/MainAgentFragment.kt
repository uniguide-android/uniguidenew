package com.example.uniguide2

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.data.Agent
import com.example.uniguide2.databinding.FragmentMainAgentBinding
import com.example.uniguide2.viewmodel.AgentViewModel

private lateinit var mainBinding: FragmentMainAgentBinding
private lateinit var agentViewModel: AgentViewModel
private lateinit var options : NavOptions

lateinit var sharedPref: SharedPreferences

private var listener: MainAgentFragment.OnFragmentInteractionListener? = null



class MainAgentFragment:Fragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mainBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_main_agent,container , false)
        val myView : View = mainBinding.root
        mainBinding.callback = this

        options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }


        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel::class.java)

        sharedPref=(activity as Activity).getSharedPreferences(SHARED_PREFERENCE_ID, Context.MODE_PRIVATE)
        val userId=sharedPref.getLong(ID_KEY,0)
        if(connected()){
            agentViewModel.findByUserId(userId)
            agentViewModel.getByUserResponse.observe(this, Observer { response ->
                response.body()?.run {
                    updateFields(this)
                }
            })
        }
        else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }
        return myView
    }


    private fun updateFields(agent: Agent) {
        mainBinding.usernameTextview.setText(agent.name)
        mainBinding.contentTextview.setText(agent.email)
        mainBinding.uniNewsTitle.setText(agent.phoneNo)
        mainBinding.uniNewsContent.setText(agent.address)
        mainBinding.descriptionTv.setText(agent.description)
    }

    private fun connected():Boolean {

        val connectivityManager = (activity as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    fun addNews(){
        findNavController().navigate(R.id.agentPostFragment, null, options)

    }
}