package com.example.uniguide2

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.data.University
import com.example.uniguide2.databinding.FragmentMainUniversityBinding
import com.example.uniguide2.viewmodel.UniversityViewModel

private lateinit var uniBinding: FragmentMainUniversityBinding
private lateinit var uniViewModel: UniversityViewModel
private lateinit var options : NavOptions

lateinit var sharedPrefer: SharedPreferences

private var listener: MainUniversityFragment.OnFragmentInteractionListener? = null


class MainUniversityFragment : Fragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        /*val view= inflater.inflate(R.layout.fragment_main_university, container, false)
        name=view.username_textview
        website=view.content_textview

        phone=view.uni_news_title
        description=view.description_tv*/

        uniBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_main_university,container , false)
        val myView : View = uniBinding.root
        uniBinding.callback = this

        options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }


        uniViewModel = ViewModelProviders.of(this).get(UniversityViewModel::class.java)

        sharedPrefer=(activity as Activity).getSharedPreferences(SHARED_PREFERENCE_ID, Context.MODE_PRIVATE)
        val userId=sharedPrefer.getLong(ID_KEY,0)
        if(connected()){
            uniViewModel.findByUserId(userId)
            uniViewModel.getByUserResponse.observe(this, Observer { response ->
                response.body()?.run {
                    updateFields(this)
                }
            })
        }
        return myView
    }


    private fun updateFields(uni: University) {
        uniBinding.usernameTextview.setText(uni.name)
        uniBinding.contentTextview.setText(uni.website)
        uniBinding.uniNewsTitle.setText(uni.phoneNo)
        uniBinding.descriptionTv.setText(uni.description)
    }

    private fun connected():Boolean {

        val connectivityManager = (activity as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }
    fun uniPost(){
        findNavController().navigate(R.id.uniPostFragment, null, options)
    }

}