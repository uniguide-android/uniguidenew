package com.example.uniguide2

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.data.Post
import com.example.uniguide2.data.Student
import com.example.uniguide2.data.User
import com.example.uniguide2.databinding.FragmentPostBinding
import com.example.uniguide2.viewmodel.PostViewModel
import com.example.uniguide2.viewmodel.StudentViewModel
import com.example.uniguide2.viewmodel.UserViewModel


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PostFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [PostFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */

class PostFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null


    private lateinit var postBinding: FragmentPostBinding
    private lateinit var userViewModel: UserViewModel
    private lateinit var postViewModel: PostViewModel
    private lateinit var studentViewModel: StudentViewModel
    private lateinit var user: User


    private lateinit var options : NavOptions


    lateinit var usernameTextView: TextView
    lateinit var postButton: Button
    lateinit var postEditText: EditText

    lateinit var sharedPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

       postBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_post,container , false)
        val myView : View = postBinding.root
        postBinding.callback = this

        sharedPref = (listener as Activity).getSharedPreferences(SHARED_PREFERENCE_ID, Context.MODE_PRIVATE)

        val userId = sharedPref.getLong(ID_KEY, 0)
        postViewModel = ViewModelProviders.of(this).get(PostViewModel::class.java)
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        studentViewModel = ViewModelProviders.of(this).get(StudentViewModel::class.java)

        if(connected()){
            userViewModel.findById(userId)
            userViewModel.getByIdResponse.observe(this, Observer { response ->
                response.body()?.run{
                    user = this
                    postBinding.postUsernameTextView.text = this.username
                }
            })


        }else{
            Toast.makeText(activity,"Please connect to the Internet to proceed",Toast.LENGTH_SHORT).show()

        }

        options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }


        return myView
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PostFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PostFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun readFields(student: Student): Post {
        var content = postBinding.postEditText.text.toString()
        return Post(0, content, "false", "universities", 0, student.user.username)
    }

    private fun connected():Boolean {

        val connectivityManager = (listener as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }
    fun addPost(){
        var student: Student
        if(connected()){
            studentViewModel.findByUserId(user.id)
            studentViewModel.getByUserResponse.observe(this, Observer{response ->
                val owner = this
                response.body()?.run{
                    var post = readFields(this)

                    postViewModel.savePost(post)
                    postViewModel.insertResponse.observe(owner, Observer { response ->
                        response.body()?.run{
                            findNavController().navigate(R.id.universitiesMainFragment, null, options)
                        }
                    })
                }

            })


        }else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }
    }
}
