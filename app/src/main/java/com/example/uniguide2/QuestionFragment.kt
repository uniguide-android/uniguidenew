package com.example.uniguide2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.uniguide2.data.CareerQuestion
import com.example.uniguide2.databinding.FragmentQuestionBinding
import com.example.uniguide2.viewmodel.CareerQuestionViewModel

private lateinit var myContainer:ViewGroup
private lateinit var viewModel: CareerQuestionViewModel
private lateinit var binding: FragmentQuestionBinding

class QuestionFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater ,R.layout.fragment_question,container , false)
        var myView : View = binding.root
        binding.callback = this
        viewModel = ViewModelProviders.of(this).get(CareerQuestionViewModel::class.java)

        container?.let{
            myContainer = it;
        }

        return myView
    }

    private fun getGroup(binding: FragmentQuestionBinding) : CareerQuestion {
        return CareerQuestion(binding.codeText.text.toString(),binding.descrText.text.toString());
    }
    private fun getUpdatedGroup(binding: FragmentQuestionBinding): CareerQuestion{
        return CareerQuestion(binding.codeText.text.toString(),binding.searchText.text.toString());
    }
    fun clearFields(binding: FragmentQuestionBinding){
        binding.codeText.setText("")
        binding.descrText.setText("")
        binding.searchText.setText("")
    }
    fun setFields(binding: FragmentQuestionBinding,group: CareerQuestion){
        binding.codeText.setText(group.careerCode)
    }
    fun addQuestion() {
        binding?.let {
            Toast.makeText(myContainer?.context, binding.codeText.text.toString()+ " "+binding.descrText.text.toString(), Toast.LENGTH_SHORT).show()

            viewModel.addCareerQuestion(getGroup(binding))
            Toast.makeText(myContainer?.context, "Career Question has been added successfully", Toast.LENGTH_SHORT)
                .show()
            clearFields(binding)
        }
    }

    fun updateQuestion(){
        binding?.let{
            if(!binding.searchText.text.toString().isNullOrEmpty()) {
                viewModel.updateCareerQuestion(getUpdatedGroup(it))
                Toast.makeText(myContainer?.context, "Career Question has been updated successfully", Toast.LENGTH_SHORT)
                    .show()
            }
            else{
                Toast.makeText(myContainer?.context, "Can't add Career Question", Toast.LENGTH_SHORT)
                    .show()
            }
            binding.question = null
        }
    }

    fun deleteQuestion(){
        binding?.let{
            viewModel.deleteCareerQuestion(getGroup(it))
            Toast.makeText(myContainer?.context,"Career Question has been deleted successfully", Toast.LENGTH_SHORT).show()
            clearFields(it)
        }

    }

    fun searchQuestion(){
        val codeSearch = binding.searchText.text.toString()
        viewModel.getGroupByQuestion(codeSearch).observe(this, Observer{
                group->group?.let {
            setFields(binding,group)
        }
        })

    }


}