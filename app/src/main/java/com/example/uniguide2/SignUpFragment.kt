package com.example.uniguide2

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.data.Student
import com.example.uniguide2.data.User
import com.example.uniguide2.databinding.FragmentSignupBinding
import com.example.uniguide2.viewmodel.StudentViewModel

private lateinit var signupBinding: FragmentSignupBinding
private lateinit var studentViewModel: StudentViewModel
private lateinit var options : NavOptions

private var listener: SignUpFragment.OnFragmentInteractionListener? = null


class SignUpFragment : Fragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        signupBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_signup,container , false)
        val myView : View = signupBinding.root
        signupBinding.callback = this

        studentViewModel = ViewModelProviders.of(this).get(StudentViewModel::class.java)


        options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }



        return myView
    }


    private fun readFields(): Student {
        var user = User(0, signupBinding.signupUsernameEditText.text.toString(), signupBinding.signupPasswordEditText.text.toString(), emptySet())
        return Student(0, signupBinding.nameEditText.text.toString(), signupBinding.emailEditText.text.toString(), signupBinding.schoolEditText.text.toString(),
            signupBinding.gradeEditText.text.toString(), signupBinding.aboutEditText.text.toString(), user)
    }

    private fun connected():Boolean {

        val connectivityManager = (activity as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    fun signStudent() {
        val student = readFields()
        if (connected()) {
            studentViewModel.registerStudent(student)
            studentViewModel.getResponse.observe(this, Observer { response ->
                response.body()?.run {
                    findNavController().navigate(R.id.loginFragment, null, options)
                }
            })

        }
        else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }
    }

}