package com.example.uniguide2

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.data.News
import com.example.uniguide2.data.Student
import com.example.uniguide2.data.University
import com.example.uniguide2.data.User
import com.example.uniguide2.databinding.FragmentUniPostBinding
import com.example.uniguide2.viewmodel.NewsViewModel
import com.example.uniguide2.viewmodel.UniversityViewModel
import com.example.uniguide2.viewmodel.UserViewModel


private lateinit var userViewModel: UserViewModel
private lateinit var newsViewModel: NewsViewModel
private lateinit var uniViewModel: UniversityViewModel


private lateinit var options : NavOptions

private lateinit var user: User
private lateinit var postBinding : FragmentUniPostBinding

private lateinit var sharedPrefPost: SharedPreferences

private var listener: UniPostFragment.OnFragmentInteractionListener? = null


class UniPostFragment : Fragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        postBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_uni_post,container , false)
        val myView : View = postBinding.root
        postBinding.callback = this

        sharedPrefPost = (activity as Activity).getSharedPreferences(SHARED_PREFERENCE_ID, Context.MODE_PRIVATE)

        val userId = sharedPrefPost.getLong(ID_KEY, 0)

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        newsViewModel = ViewModelProviders.of(this).get(NewsViewModel::class.java)
        uniViewModel = ViewModelProviders.of(this).get(UniversityViewModel::class.java)

        if(connected()){

            userViewModel.findById(userId)
            userViewModel.getByIdResponse.observe(this, Observer { response ->
                response.body()?.run{
                    user = this
                    postBinding.usernameTextviewUni.text = this.username
                }
            })


        }else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }

        options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }

        return myView
    }

    fun readFields(uni: University): News {
        return News(0, postBinding.uniNewsTitle.text.toString(), postBinding.uniNewsContent.text.toString(), uni.user.username)
    }

    private fun connected():Boolean {

        val connectivityManager = (activity as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    fun postNews(){
        var student: Student
        if(connected()){
            uniViewModel.findByUserId(user.id)
            uniViewModel.getByUserResponse.observe(this, Observer{response ->
                val owner = this
                response.body()?.run{
                    var news = readFields(this)

                    newsViewModel.saveNews(news)
                    newsViewModel.insertResponse.observe(owner, Observer { response ->
                        response.body()?.run{
                            findNavController().navigate(R.id.mainUnifragment, null, options)
                        }
                    })
                }
            })


        }else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }

    }


}