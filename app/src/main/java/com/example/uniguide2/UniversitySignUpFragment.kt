package com.example.uniguide2

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.example.uniguide2.data.University
import com.example.uniguide2.data.User
import com.example.uniguide2.databinding.FragmentUniversitySignUpBinding
import com.example.uniguide2.viewmodel.UniversityViewModel

private lateinit var uniSignBinding: FragmentUniversitySignUpBinding
private lateinit var uniViewModel: UniversityViewModel
private lateinit var options : NavOptions

private var listener: UniversitySignUpFragment.OnFragmentInteractionListener? = null



class UniversitySignUpFragment : Fragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        uniSignBinding = DataBindingUtil.inflate(inflater ,R.layout.fragment_university_sign_up,container , false)
        val myView : View = uniSignBinding.root
        uniSignBinding.callback = this

        uniViewModel = ViewModelProviders.of(this).get(UniversityViewModel::class.java)

        options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }


        return myView
    }

    private fun readFields(): University {
        var user = User(0, uniSignBinding.userNameEt.text.toString(), uniSignBinding.passwordEt.text.toString(), emptySet())
        return University(0, uniSignBinding.NameEt.text.toString(),
            uniSignBinding.addressEt.text.toString(),
            uniSignBinding.phoneNumberEt.text.toString(),
            uniSignBinding.descriptionEt.text.toString(), user)
    }

    private fun connected():Boolean {

        val connectivityManager = (activity as Activity).getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

    fun signUniversity(){
        val uni = readFields()
        if(connected()){
            uniViewModel.registerUniversity(uni)
            uniViewModel.insertResponse.observe(this, Observer { response ->
                response.body()?.run {
                    findNavController().navigate(R.id.loginFragment, null, options)
                }
            })

        }
        else{
            Toast.makeText(activity,"Please connect to the Internet to proceed", Toast.LENGTH_SHORT).show()

        }


    }
}