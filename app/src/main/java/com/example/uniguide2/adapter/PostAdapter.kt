package com.example.uniguide2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.uniguide2.R
import com.example.uniguide2.data.Post
import com.example.uniguide2.databinding.RecyclerViewItemBinding
import com.example.uniguide2.eventHandler.PostEventHandler

class PostAdapter(val context: Context): RecyclerView.Adapter<PostAdapter.PostViewHolder>(), PostEventHandler {


    val inflater = LayoutInflater.from(context)
    private var posts: List<Post> = emptyList()

    /*var user = User(1, "lydia", "1234", emptySet())
    var student = Student(1, "Lydia", "lydia@gmail.com", "eps", "11", "univesities",  user)
    private val posts = listOf(

        Post(1,  "Lorem Ipsum", "universities","false", Date(), 0, student),
        Post(2, "Lorem Ipsum", "universities","false", Date(),0, student),
        Post(3, "Lorem Ipsum","universities", "false", Date(),0, student)
    )*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {

        val bindingUtil: RecyclerViewItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context) ,R.layout.recycler_view_item, parent, false)

        //val recyclerViewItem = inflater.inflate(R.layout.recycler_view_item, parent, false)

        return PostViewHolder(bindingUtil)
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val post = posts[position]
        holder.binding.post = post
    }

    internal fun setPosts(posts: List<Post>){
        this.posts = posts
        notifyDataSetChanged()
    }

    class PostViewHolder(binding: RecyclerViewItemBinding): RecyclerView.ViewHolder(binding.root){
        val binding = binding
    }
}