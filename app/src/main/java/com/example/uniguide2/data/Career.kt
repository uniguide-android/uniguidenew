package com.example.uniguide2.data

import androidx.room.*
import java.io.Serializable

@Entity(tableName = "career_groups")
data class Career(@PrimaryKey @ColumnInfo(name="group_code",index = true)var groupCode:String,
                  @ColumnInfo(name="group_name") var groupName:String,
                  @ColumnInfo(name="group_jobs")var occupations:String,
                  @ColumnInfo(name="group_description")var description:String) : Serializable{
}