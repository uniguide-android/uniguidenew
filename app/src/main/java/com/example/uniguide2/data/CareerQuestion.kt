package com.example.uniguide2.data

import androidx.room.*

@Entity(tableName = "career_questions",foreignKeys = [ForeignKey(entity = Career::class,
    parentColumns = arrayOf("group_code"),
    childColumns = arrayOf("career_code")
)]
)
data class CareerQuestion(@PrimaryKey @ColumnInfo(name="question_description")val careerQuestion: String,
                          @ForeignKey(entity = Career::class,
                              parentColumns = arrayOf("group_code"),
                              childColumns = arrayOf("career_code")
                          ) @ColumnInfo(name = "career_code",index = true)val careerCode:String) {
}