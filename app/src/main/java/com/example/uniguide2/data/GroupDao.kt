package com.example.uniguide2.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface GroupDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGroup(group: Career):Long

    @Query("Select * from career_groups where group_code= :code")
    fun getCareerGroupByCode(code:String):LiveData<Career>

    @Query("Select * from career_groups")
    fun getAllGroups():LiveData<List<Career>>



}