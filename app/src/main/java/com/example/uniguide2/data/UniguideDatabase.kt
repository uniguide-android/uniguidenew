package com.example.uniguide2.data

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.uniguide2.data.migrations.Migration1To2
import com.example.uniguide2.data.migrations.Migration2To3

@Database(entities = arrayOf(Career::class,CareerQuestion::class, Post::class, News::class),version=3)
abstract class UniguideDatabase : RoomDatabase(){
    abstract fun groupDao(): GroupDao
    abstract fun questionDao() : QuestionDao
    abstract fun postDao(): PostDao
    abstract fun newsDao(): NewsDao

    companion object {
        @Volatile
        private var INSTANCE: UniguideDatabase? = null

        fun getDatabase(context: Context): UniguideDatabase {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                @VisibleForTesting
                val MIGRATION_1_TO_2 = Migration1To2()
                val MIGRATION_2_TO_3 = Migration2To3()
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    UniguideDatabase::class.java, "uniguide_db")
                    .addMigrations(MIGRATION_1_TO_2, MIGRATION_2_TO_3)
                    .build()

                INSTANCE = instance
                return instance
            }

        }
    }
}