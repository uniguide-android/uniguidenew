package com.example.uniguide2.data

import java.io.Serializable

data class User(val id:Long, val username:String, val password:String, val roles:Set<Role>): Serializable {
}