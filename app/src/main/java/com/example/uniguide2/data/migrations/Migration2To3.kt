package com.example.uniguide2.data.migrations

import androidx.annotation.VisibleForTesting
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
@VisibleForTesting
class Migration2To3 : Migration(2, 3) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE post" +
                    "('id' INTEGER NOT NULL, 'content' TEXT NOT NULL, 'flag' TEXT NOT NULL, 'likes' INTEGER NOT NULL," +
                " 'type' TEXT NOT NULL, 'username' TEXT NOT NULL, PRIMARY KEY(id))")
        database.execSQL("CREATE TABLE news" +
                "('id' INTEGER NOT NULL, 'author' TEXT NOT NULL, 'content' TEXT NOT NULL, 'title' TEXT NOT NULL," +
                " PRIMARY KEY(id))")
    }
}