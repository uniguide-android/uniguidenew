package com.example.uniguide2.network

import com.example.uniguide2.data.News
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface NewsApiService {
    @GET("news/all")
    fun findAllAsync(): Deferred<Response<List<News>>>
    @GET("news/{id}")
    fun findByIdAsync(@Path("id") id: Long): Deferred<Response<News>>
    @POST("news/save")
    fun saveNewsAsync(@Body newNews: News): Deferred<Response<News>>
    @PUT("news/{id}")
    fun updateNewsAsync(@Path("id") id: Long, @Body newNews: News): Deferred<Response<News>>
    @DELETE("news/{id}")
    fun deleteNewsAsync(@Path("id") id: Long): Deferred<Response<Void>>


    companion object {

        private val baseUrl = "http://192.168.137.1:8090/"

        fun getInstance(): NewsApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(NewsApiService::class.java)
        }
    }
}