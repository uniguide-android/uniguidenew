package com.example.uniguide2.network

import com.example.uniguide2.data.Post
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface PostApiService {
    @GET("post/all")
    fun findAllAsync(): Deferred<Response<List<Post>>>
    @GET("post/{id}")
    fun findByIdAsync(@Path("id") id: Long): Deferred<Response<Post>>
    @POST("post/save")
    fun savePostAsync(@Body newPost: Post): Deferred<Response<Post>>
    @PUT("post/{id}")
    fun updatePostAsync(@Path("id") id: Long, @Body newPost: Post): Deferred<Response<Post>>
    @DELETE("post/{id}")
    fun deletePostAsync(@Path("id") id: Long): Deferred<Response<Void>>


    companion object {

        private val baseUrl = "http://192.168.137.1:8090/"

        fun getInstance(): PostApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(PostApiService::class.java)
        }
    }
}