package com.example.uniguide2.network

import com.example.uniguide2.data.Student
import com.example.uniguide2.data.User
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface UserApiService {

    @POST("login")
    fun login(@Query("username") username: String, @Query("password") password: String): Deferred<Response<User>>

    @GET("user/{id}")
    fun findByIdAsync(@Path("id") id: Long): Deferred<Response<User>>

    companion object {

        private val baseUrl = "http://192.168.137.1:8090/"

        fun getInstance(): UserApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(UserApiService::class.java)
        }
    }
}