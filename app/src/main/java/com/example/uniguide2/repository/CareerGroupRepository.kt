package com.example.uniguide2.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.uniguide2.data.Career
import com.example.uniguide2.data.GroupDao
import com.example.uniguide2.network.CareerApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class CareerGroupRepository(private val groupDao: GroupDao, private val careerGroupApiService: CareerApiService) {

    fun insertGroupRoom(group:Career):Long{
        return groupDao.insertGroup(group)
    }

    fun allGroupsRoom():LiveData<List<Career>>{
        return groupDao.getAllGroups()
    }

    fun getAGroup(code:String):LiveData<Career>{
        return groupDao.getCareerGroupByCode(code)
    }

    suspend fun insertGroupApi(group:Career){
        Log.d("Repo", group.groupName)
        withContext(Dispatchers.IO) {
            careerGroupApiService.saveCareerGroupAsync(group).await()
        }
    }

    fun deleteGroupApi(group:Career){
        Log.d("Group",group.groupName)
        careerGroupApiService.deleteCareerGroupAsync(group.groupCode)
    }

    fun updateGroupApi(group:Career){
        careerGroupApiService.updateCareerGroupAsync(group.groupCode, group)
    }

    suspend fun getGroupByCodeApi(code:String){
        val response: Response<Career> = careerGroupApiService.findByIdAsync(code).await()
        val group = response.body()
        if(group != null){
            groupDao.insertGroup(group)
        }
    }

    suspend fun allGroupsApi(){
        val response: Response<List<Career>> = careerGroupApiService.findAllAsync().await()
        val groups = response.body()
        if(groups != null){
            Log.d("Repo all" , groups[0].groupName)
            for(group in groups){
                groupDao.insertGroup(group)
            }
        }
    }
}