package com.example.uniguide2.repository

import com.example.uniguide2.data.News
import com.example.uniguide2.data.NewsDao
import com.example.uniguide2.network.NewsApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class NewsRepository(private val newsDao: NewsDao, private val newsApiService: NewsApiService) {

    suspend fun saveNewsApi( newNews: News): Response<News> =
        withContext(Dispatchers.IO) {
            newsApiService.saveNewsAsync(newNews).await()
        }

    fun saveNewsRoom( newNews: News): Long =
        newsDao.insertNews(newNews)

    suspend fun findAllApi() =
        withContext(Dispatchers.IO) {
            val response: Response<List<News>> = newsApiService.findAllAsync().await()
            val newss = response.body()
            if(newss != null){
                for(news in newss){
                    saveNewsRoom(news)
                }
            }
        }

    suspend fun findByIdApi(id: Long): Response<News> =
        withContext(Dispatchers.IO) {
            newsApiService.findByIdAsync(id).await()
        }

    suspend fun updateNewsApi(id: Long, newNews: News): Response<News> =
        withContext(Dispatchers.IO) {
            newsApiService.updateNewsAsync(id, newNews).await()
        }

    suspend fun deleteNewsApi(id: Long): Response<Void> =
        withContext(Dispatchers.IO) {
            newsApiService.deleteNewsAsync(id).await()
        }

    suspend fun findAll(): List<News> =
        withContext(Dispatchers.IO){
            newsDao.getAllNews()
        }
}