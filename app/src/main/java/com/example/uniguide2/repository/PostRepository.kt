package com.example.uniguide2.repository

import com.example.uniguide2.data.Post
import com.example.uniguide2.data.PostDao
import com.example.uniguide2.network.PostApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class PostRepository(private val postDao: PostDao, private val postApiService: PostApiService) {

    suspend fun savePostApi( newPost: Post): Response<Post> =
        withContext(Dispatchers.IO) {
            postApiService.savePostAsync(newPost).await()
        }

    fun savePostRoom( newPost: Post): Long =
        postDao.insertPost(newPost)

    suspend fun findAllApi() =
        withContext(Dispatchers.IO) {
            val response: Response<List<Post>> = postApiService.findAllAsync().await()
            val posts = response.body()
            if(posts != null){
                for(post in posts){
                    savePostRoom(post)
                }
            }
        }

    suspend fun findByIdApi(id: Long): Response<Post> =
        withContext(Dispatchers.IO) {
            postApiService.findByIdAsync(id).await()
        }

    suspend fun updatePostApi(id: Long, newPost: Post): Response<Post> =
        withContext(Dispatchers.IO) {
            postApiService.updatePostAsync(id, newPost).await()
        }

    suspend fun deletePostApi(id: Long): Response<Void> =
        withContext(Dispatchers.IO) {
            postApiService.deletePostAsync(id).await()
        }
    suspend fun findAll(): List<Post> =
        withContext(Dispatchers.IO){
            postDao.getAllPosts()
        }
}