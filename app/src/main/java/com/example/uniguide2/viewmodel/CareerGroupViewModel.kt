package com.example.uniguide2.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import androidx.lifecycle.*
import com.example.uniguide2.data.Career
import com.example.uniguide2.repository.CareerGroupRepository
import com.example.uniguide2.data.UniguideDatabase
import com.example.uniguide2.network.CareerApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CareerGroupViewModel(application:Application):AndroidViewModel(application) {
    private val groupRepository: CareerGroupRepository
    val allGroups : LiveData<List<Career>>
    private val filterData = MutableLiveData<String>()
    private lateinit var searchData: LiveData<Career>

    init {
        val dao = UniguideDatabase.getDatabase(application).groupDao()
        val apiService = CareerApiService.getInstance()
        groupRepository = CareerGroupRepository(dao, apiService)
        if(connected(getApplication())){
            viewModelScope.launch (Dispatchers.IO){
                groupRepository.allGroupsApi()
            }
        }
        allGroups = groupRepository.allGroupsRoom()
    }


    fun addCareerGroup(group:Career)=viewModelScope.launch (Dispatchers.IO){
        Log.d("Add", group.groupName)
        if(connected(getApplication())){
            groupRepository.insertGroupApi(group)
        }
    }

    fun deleteCareerGroup(group:Career)=viewModelScope.launch (Dispatchers.IO) {
        if(connected(getApplication())){
            groupRepository.deleteGroupApi(group)
        }
    }

    fun updateCareerGroup(group:Career) = viewModelScope.launch (Dispatchers.IO){
        if(connected(getApplication())){
            groupRepository.updateGroupApi(group)
        }
    }

    fun getGroupByCode(code: String):LiveData<Career>{
        filterData.value=code
        searchData= Transformations.switchMap(filterData){
            if (connected(getApplication())){
                viewModelScope.launch(Dispatchers.IO) {
                    groupRepository.getGroupByCodeApi(it)
                }
                groupRepository.getAGroup(it)
            }else{
                groupRepository.getAGroup(it)
            }

        }
        return searchData

    }

    private fun connected(context: Context):Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }





}