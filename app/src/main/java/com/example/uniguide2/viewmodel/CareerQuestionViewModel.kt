package com.example.uniguide2.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.example.uniguide2.data.CareerQuestion
import com.example.uniguide2.data.UniguideDatabase
import com.example.uniguide2.repository.CareerQuestionRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CareerQuestionViewModel(application: Application):AndroidViewModel(application) {
    private val questionRepository:CareerQuestionRepository
    private val allQuestions : LiveData<List<CareerQuestion>>
    private val filterData = MutableLiveData<String>()
    private lateinit var searchData: LiveData<CareerQuestion>

    init {
        val dao = UniguideDatabase.getDatabase(application).questionDao()
        questionRepository = CareerQuestionRepository(dao)
        allQuestions = questionRepository.allQuestions()

    }

    fun addCareerQuestion(question:CareerQuestion)=viewModelScope.launch (Dispatchers.IO){
       questionRepository.insertQuestion(question)
    }

    fun deleteCareerQuestion(question: CareerQuestion)=viewModelScope.launch (Dispatchers.IO) {
        questionRepository.deleteQuestion(question)
    }

    fun updateCareerQuestion(question: CareerQuestion) = viewModelScope.launch (Dispatchers.IO){
        questionRepository.updateQuestion(question)
    }

    fun getGroupByQuestion(code: String):LiveData<CareerQuestion>{
        filterData.value=code
        searchData= Transformations.switchMap(filterData){
            questionRepository.getQuestionByDescr(it)
        }
        return searchData

    }
}