package com.example.uniguide2.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.lifecycle.*
import com.example.uniguide2.data.News
import com.example.uniguide2.data.UniguideDatabase
import com.example.uniguide2.network.NewsApiService
import com.example.uniguide2.repository.NewsRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class NewsViewModel(application: Application): AndroidViewModel(application) {

    private val newsRepository: NewsRepository

    init{
        val newsApiService = NewsApiService.getInstance()
        val dao = UniguideDatabase.getDatabase(application).newsDao()
        newsRepository = NewsRepository(dao, newsApiService)
    }

    private  val _getResponse = MutableLiveData<Response<News>>()
    val getResponse: LiveData<Response<News>>
        get() = _getResponse

    private  val _getResponses = MutableLiveData<List<News>>()
    val getResponses: LiveData<List<News>>
        get() = _getResponses

    private val _updateResponse = MutableLiveData<Response<News>>()
    val updateResponse: LiveData<Response<News>>
        get() = _updateResponse

    private val _insertResponse = MutableLiveData<Response<News>>()
    val insertResponse: LiveData<Response<News>>
        get() = _insertResponse

    private val _deleteResponse = MutableLiveData<Response<Void>>()
    val deleteResponse: MutableLiveData<Response<Void>>
        get() = _deleteResponse

    fun findById(id: Long) = viewModelScope.launch {
        _getResponse.postValue(newsRepository.findByIdApi(id))
    }

    fun saveNews(newNews: News) = viewModelScope.launch {
        _insertResponse.postValue(newsRepository.saveNewsApi(newNews))
    }

    fun updateNews(id: Long, newNews: News) = viewModelScope.launch {
        _updateResponse.postValue(newsRepository.updateNewsApi(id, newNews))
    }

    fun findAll() = viewModelScope.launch {
        if(connected(getApplication())){
            newsRepository.findAllApi()
        }
        _getResponses.postValue(newsRepository.findAll())
    }

    fun deleteNews(id: Long) = viewModelScope.launch {
        _deleteResponse.postValue(newsRepository.deleteNewsApi(id))
    }

    private fun connected(context: Context):Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }

}