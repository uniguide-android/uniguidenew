package com.example.uniguide2.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.lifecycle.*
import com.example.uniguide2.data.Post
import com.example.uniguide2.data.UniguideDatabase
import com.example.uniguide2.network.PostApiService
import com.example.uniguide2.repository.PostRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class PostViewModel(application: Application): AndroidViewModel(application) {

    private val postRepository: PostRepository

    init{
        val postApiService = PostApiService.getInstance()
        val dao = UniguideDatabase.getDatabase(application).postDao()
        postRepository = PostRepository(dao, postApiService)
    }

    private  val _getResponse = MutableLiveData<Response<Post>>()
    val getResponse: LiveData<Response<Post>>
        get() = _getResponse

    private  val _getResponses = MutableLiveData<List<Post>>()
    val getResponses: LiveData<List<Post>>
        get() = _getResponses

    private val _updateResponse = MutableLiveData<Response<Post>>()
    val updateResponse: LiveData<Response<Post>>
        get() = _updateResponse

    private val _insertResponse = MutableLiveData<Response<Post>>()
    val insertResponse: LiveData<Response<Post>>
        get() = _insertResponse

    private val _deleteResponse = MutableLiveData<Response<Void>>()
    val deleteResponse: MutableLiveData<Response<Void>>
        get() = _deleteResponse

    fun findById(id: Long) = viewModelScope.launch {
        _getResponse.postValue(postRepository.findByIdApi(id))
    }

    fun savePost(newPost: Post) = viewModelScope.launch {
        _insertResponse.postValue(postRepository.savePostApi(newPost))
    }

    fun updatePost(id: Long, newPost: Post) = viewModelScope.launch {
        _updateResponse.postValue(postRepository.updatePostApi(id, newPost))
    }

    fun findAll() = viewModelScope.launch {
        if(connected(getApplication())){
            postRepository.findAllApi()
        }
        _getResponses.postValue(postRepository.findAll())
    }

    fun deletePost(id: Long) = viewModelScope.launch {
        _deleteResponse.postValue(postRepository.deletePostApi(id))
    }

    private fun connected(context: Context):Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }
}