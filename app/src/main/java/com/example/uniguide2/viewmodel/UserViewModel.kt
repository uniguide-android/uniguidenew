package com.example.uniguide2.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.uniguide2.data.Agent
import com.example.uniguide2.data.User
import com.example.uniguide2.network.AgentApiService
import com.example.uniguide2.network.UserApiService
import com.example.uniguide2.repository.AgentRepository
import com.example.uniguide2.repository.UserRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class UserViewModel : ViewModel() {

    private val userRepository: UserRepository

    init{
        val userApiService = UserApiService.getInstance()
        userRepository = UserRepository(userApiService)
    }

    private  val _getResponse = MutableLiveData<Response<User>>()
    val getResponse: LiveData<Response<User>>
        get() = _getResponse

    private  val _getByIdResponse = MutableLiveData<Response<User>>()
    val getByIdResponse: LiveData<Response<User>>
        get() = _getByIdResponse

    fun login(username: String, password: String) = viewModelScope.launch {
        _getResponse.postValue(userRepository.login(username, password))
    }

    fun findById(id: Long) = viewModelScope.launch {
        _getByIdResponse.postValue(userRepository.findById(id))
    }
}