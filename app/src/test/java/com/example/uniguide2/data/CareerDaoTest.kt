package com.example.uniguide2.data

import androidx.lifecycle.LiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers.notNullValue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class CareerDaoTest {

    private lateinit var database: UniguideDatabase

    @Before
    fun initDb() {

        database =
            Room.databaseBuilder(
                ApplicationProvider.getApplicationContext(),
                UniguideDatabase::class.java,
                "uniguide_db"
            ).allowMainThreadQueries().build()
    }

    @After
    fun closeDb() = database.close()


    @Test
    fun insertGroupAndGetByCode() = runBlockingTest {
        // GIVEN - insert a group
        val group = Career(
            "B",
            "Finance, Banking, Investments and Insurance",
            "Accounting Clerk\n" +
                    "Appraiser, Credit Analyst, Credit Checker, Economist",
            "Interest in financial and investment planning and management, and providing banking and insurance services."
        )
        database.groupDao().insertGroup(group)

        // WHEN - Get the group by code from the database
        val loaded = database.groupDao().getCareerGroupByCode(group.groupCode)

        // THEN - The loaded data contains the expected values
        MatcherAssert.assertThat(loaded , CoreMatchers.notNullValue())

    }
}


