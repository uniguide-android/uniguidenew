package com.example.uniguide2.data

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class NewsDaoTest {
    private lateinit var database: UniguideDatabase

    @Before
    fun initDb() {

        database=
            Room.databaseBuilder(
                ApplicationProvider.getApplicationContext(),
                UniguideDatabase::class.java,
                "uniguide_db").allowMainThreadQueries().build()
    }

    @After
    fun closeDb() = database.close()


    @Test
    fun insertNewsAndGetAll() = runBlockingTest {
        // GIVEN - insert a group
        val news = News(0, "News 1","News Content","user")
        database.newsDao().insertNews(news)

        // WHEN - Get the group by code from the database
        val loaded = database.newsDao().getAllNews()[0]

        // THEN - The loaded data contains the expected values
        MatcherAssert.assertThat<News>(loaded, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(loaded.id, CoreMatchers.`is`(news.id))
        MatcherAssert.assertThat(loaded.title, CoreMatchers.`is`(news.title))
        MatcherAssert.assertThat(loaded.content, CoreMatchers.`is`(news.content))
        MatcherAssert.assertThat(loaded.author, CoreMatchers.`is`(news.author))
    }
}