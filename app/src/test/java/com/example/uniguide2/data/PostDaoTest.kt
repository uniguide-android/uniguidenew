package com.example.uniguide2.data

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class PostDaoTest {
    private lateinit var database: UniguideDatabase

    @Before
    fun initDb() {

        database=
            Room.databaseBuilder(
                ApplicationProvider.getApplicationContext(),
                UniguideDatabase::class.java,
                "uniguide_db").allowMainThreadQueries().build()
    }

    @After
    fun closeDb() = database.close()


    @Test
    fun insertPostAndGetAll() = runBlockingTest {
        // GIVEN - insert a group
        val post = Post(1, "New Post","universities","false", 0, "user")
        database.postDao().insertPost(post)

        // WHEN - Get the group by code from the database
        val loaded = database.postDao().getAllPosts()[0]

        // THEN - The loaded data contains the expected values
        MatcherAssert.assertThat<Post>(loaded, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(loaded.id, CoreMatchers.`is`(post.id))
        MatcherAssert.assertThat(loaded.content, CoreMatchers.`is`(post.content))
        MatcherAssert.assertThat(loaded.username, CoreMatchers.`is`(post.username))
    }
}