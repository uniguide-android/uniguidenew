package com.example.uniguide2.repository

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.example.uniguide2.data.Career
import com.example.uniguide2.data.UniguideDatabase
import com.example.uniguide2.network.CareerApiService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.junit.*
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@MediumTest

class CareerRepositoryTest {

    private lateinit var repo: CareerGroupRepository
    private lateinit var database: UniguideDatabase

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext<Context>(),
            UniguideDatabase::class.java).allowMainThreadQueries().build()

        repo = CareerGroupRepository(database.groupDao(), CareerApiService.getInstance())
    }

    @After
    fun cleanUp() {
        database.close()
    }
    @Test
    fun insertAndRetrieve()= runBlocking{
        // GIVEN - a new group saved in the database
        val group = Career("B", "Finance","Accounting Clerk, " +
                "Appraiser","Interest in financial and investment planning and management, and providing banking and insurance services.")
        repo.insertGroupRoom(group)

        // WHEN  - Group retrieved by code
        val result  = repo.getAGroup(group.groupCode)

        Assert.assertThat(result, CoreMatchers.notNullValue())

        // THEN - Same group is returned

    }

    @Test
    fun getGroups_retrieveGroups() = runBlockingTest {
        // Given 2 new tasks in the persistent repository

        val newGroup1 = Career("B", "Finance","Accounting Clerk, " +
                "Appraiser","Interest in financial and investment planning and management, and providing banking and insurance services.")

        val newGroup2 = Career("C", "Plants, Agriculture and Natural Resources","Farm Equipment Mechanic, Fish and Game Warden, Forester, Veterinarian, Zoologist","Interest in activities involving plants, usually in an outdoor setting.")


        repo.insertGroupRoom(newGroup1)
        repo.insertGroupRoom(newGroup2)
        // Then the tasks can be retrieved from the persistent repository
        val latch = CountDownLatch(1)
        val results = repo.allGroupsRoom()
        var size = 0
        val tasks = results.observeForever{
            size = it.size
            latch.countDown()
        }
        latch.await(3,TimeUnit.SECONDS)
        Assert.assertThat(size, CoreMatchers.`is`(2))
    }

}