package com.example.uniguide2.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.example.uniguide2.data.Career
import com.example.uniguide2.data.News
import com.example.uniguide2.data.UniguideDatabase
import com.example.uniguide2.network.CareerApiService
import com.example.uniguide2.network.NewsApiService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.*
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


/**
 * Created by Hanit on 7/9/2019.
 */
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@MediumTest

class NewsRepositoryTest {

    private lateinit var repo: NewsRepository
    private lateinit var database: UniguideDatabase

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            UniguideDatabase::class.java,
            "uniguide_db").allowMainThreadQueries().build()

        repo = NewsRepository(database.newsDao(), NewsApiService.getInstance())
    }

    @After
    fun cleanUp() {
        database.close()
    }
    @Test
    fun insertAndRetrieve()= runBlocking{
        // GIVEN - a new group saved in the database
        val group = News(0, "News 1","News Content","user")
        repo.saveNewsRoom(group)

        // WHEN  - Group retrieved by code
        val result  = repo.findAll()[0]

        Assert.assertThat(result, CoreMatchers.notNullValue())

        // THEN - Same group is returned
        Assert.assertThat(result.title, CoreMatchers.`is`("News 1"))
        Assert.assertThat(result.content, CoreMatchers.`is`("News Content"))
        Assert.assertThat(result.author, CoreMatchers.`is`("user"))

    }
}