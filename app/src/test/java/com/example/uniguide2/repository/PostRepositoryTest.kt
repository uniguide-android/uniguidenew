package com.example.uniguide2.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.example.uniguide2.data.News
import com.example.uniguide2.data.Post
import com.example.uniguide2.data.UniguideDatabase
import com.example.uniguide2.network.NewsApiService
import com.example.uniguide2.network.PostApiService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.*
import org.junit.runner.RunWith


/**
 * Created by Hanit on 7/9/2019.
 */
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@MediumTest

class PostRepositoryTest {

    private lateinit var repo: PostRepository
    private lateinit var database: UniguideDatabase

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            UniguideDatabase::class.java,
            "uniguide_db").allowMainThreadQueries().build()

        repo = PostRepository(database.postDao(), PostApiService.getInstance())
    }

    @After
    fun cleanUp() {
        database.close()
    }
    @Test
    fun insertAndRetrieve()= runBlocking{
        // GIVEN - a new group saved in the database
        val group = Post(1, "New Post","universities","false", 0, "user")
        repo.savePostRoom(group)

        // WHEN  - Group retrieved by code
        val result  = repo.findAll()[0]

        Assert.assertThat(result, CoreMatchers.notNullValue())

        // THEN - Same group is returned
        Assert.assertThat(result.content, CoreMatchers.`is`("New Post"))
        Assert.assertThat(result.type, CoreMatchers.`is`("universities"))
        Assert.assertThat(result.flag, CoreMatchers.`is`("flag"))
        Assert.assertThat(result.likes, CoreMatchers.`is`(0))
        Assert.assertThat(result.flag, CoreMatchers.`is`("user"))

    }
}