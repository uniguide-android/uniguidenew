package com.example.uniguide2.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.example.uniguide2.MainCoroutineRule
import com.example.uniguide2.data.Career
import com.example.uniguide2.data.UniguideDatabase
import com.example.uniguide2.network.CareerApiService
import com.example.uniguide2.repository.CareerGroupRepository
import com.example.uniguide2.viewmodel.CareerGroupViewModel
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@ExperimentalCoroutinesApi
@MediumTest
class CareerViewModelTest {
    // Subject under test
    private lateinit var careerGroupViewModel: CareerGroupViewModel

    private lateinit var database: UniguideDatabase

    private lateinit var groupRepository:CareerGroupRepository

    private lateinit var result : Career


    // Set the main coroutines dispatcher for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    var group = Career("B", "Finance","Accounting Clerk, " +
            "Appraiser","Interest in financial and investment planning and management, and providing banking and insurance services.")

    @Before
    fun setupViewModel() {
        database=
            Room.databaseBuilder(
                ApplicationProvider.getApplicationContext(),
                UniguideDatabase::class.java,
                "uniguide_db").allowMainThreadQueries().build()

        // We initialise the repository with no tasks
        groupRepository = CareerGroupRepository(database.groupDao(), CareerApiService.getInstance())

        // Create class under test
        careerGroupViewModel = CareerGroupViewModel(ApplicationProvider.getApplicationContext())


    }

    @After
    fun closeDb() = database.close()

    @Test
    fun saveNewGroupToRepository()= runBlockingTest{
        val newName = "New Name"
        val newDescription = "Some Description"
        (careerGroupViewModel.apply {
            group.groupName = newName
            group.description = newDescription
        })
        careerGroupViewModel.addCareerGroup(group)


        // Then a task is saved in the repository

        val groups=careerGroupViewModel.allGroups

        Truth.assertThat(groups).isNotNull()
    }

    @Test
    fun deleteGroupFromRepository(){

        // Given group is in repository
        careerGroupViewModel.apply {
            this.addCareerGroup(group)
        }

        // when group is removed
       // careerGroupViewModel.deleteCareerGroup(group)


        // Then a task is deleted in the repository
       // val deletedGroup = careerGroupViewModel.getGroupByCode(group.groupCode).value

//        Truth.assertThat(deletedGroup).isNull()


    }
    @Test
    fun updateGroupFromRepository(){
        val new = "This is a new name"
        // Given group is in repository
        careerGroupViewModel.apply {
            addCareerGroup(group)
        }
        group.groupName = new
        // when group is removed
        careerGroupViewModel.updateCareerGroup(group)



        // Then a task is updated in the repository
        Truth.assertThat(group.groupName).isEqualTo(new)


    }
}